package Interface;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import Plateau.Plateau;
import Robots.Char;
import Robots.Piegeur;
import Robots.Robot;
import Robots.Tireur;

@SuppressWarnings("serial")
public class ChoixUnites extends JPanel {

	int robotChoisi = 0;
	int tireurChoisi = 0;
	int piegeurChoisi = 0;
	int charChoisi = 0;
	boolean choixDeux = false;

	/**
	 * Constructeur pour la fen�tre de choix des unit�s, o� l'un puis l'autre des joueurs doit choisir quels robots prendre dans son �quipe
	 * @param p
	 */
	
	public ChoixUnites(final Plateau p) {

		final ArrayList<Robot> equipe1 = new ArrayList<Robot>(p.nbRobots);
		final ArrayList<Robot> equipe2 = new ArrayList<Robot>(p.nbRobots);

		final JFrame f = new JFrame("VirtualWar");
		f.setResizable(false);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JLabel jLabel1 = new JLabel();
		final JLabel jLabel2 = new JLabel();
		final JLabel jLabel3 = new JLabel();
		final JLabel jLabel4 = new JLabel();
		final JLabel jLabel5 = new JLabel();

		jLabel1.setIcon(new ImageIcon("simont.jpg"));
		jLabel1.setBounds(0, 0, 1000, 730);
		jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

		jLabel2.setFont(new Font("Georgia", 1, 24));
		jLabel2.setText("Joueur 1 : Il vous reste " + p.nbRobots
				+ " robots a� choisir !");
		jLabel2.setForeground(Color.YELLOW);
		jLabel2.setBounds(75, 50, 600, 80);
		jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

		jLabel3.setFont(new Font("Georgia", 0, 22));
		jLabel3.setText("Tireur : " + tireurChoisi);
		jLabel3.setBounds(275, 150, 200, 50);
		jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

		jLabel4.setFont(new Font("Georgia", 0, 22));
		jLabel4.setText("Piegeur : " + piegeurChoisi);
		jLabel4.setBounds(275, 250, 200, 50);
		jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

		jLabel5.setFont(new Font("Georgia", 0, 22));
		jLabel5.setText("Char : " + charChoisi);
		jLabel5.setBounds(275, 350, 200, 50);
		jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

		jLabel3.addMouseListener(new MouseAdapter() {

			public void mouseEntered(MouseEvent evt) {
				jLabel3.setFont(new Font("Georgia", 1, 22));
			}

			public void mouseExited(MouseEvent evt) {
				jLabel3.setFont(new Font("Georgia", 0, 22));
			}

			public void mousePressed(MouseEvent e) {
				robotChoisi++;
				tireurChoisi++;

				if (!choixDeux) {
					Tireur tireur = new Tireur(p.getCoordBase(1), 1, p);
					equipe1.add(tireur);
					p.getBase1().poserRobot(tireur);

				} else {
					Tireur tireur = new Tireur(p.getCoordBase(2), 2, p);
					equipe2.add(tireur);
					p.getBase2().poserRobot(tireur);

				}

				if (p.nbRobots - robotChoisi == 0) {
					if (!choixDeux) {
						if (p.modeChoisi == 1) {
							choixDeux = true;
							robotChoisi = 0;
							tireurChoisi = 0;
							piegeurChoisi = 0;
							charChoisi = 0;
							jLabel4.setText("Piegeur : " + piegeurChoisi);
							jLabel5.setText("Char : " + charChoisi);
						} else {
							f.dispose();

							// Constitution aleatoire de l'equipe 2.

							for (int i = 1; i <= p.nbRobots; i++) {
								int robotChoisi = new Random().nextInt(3) + 1;
								switch (robotChoisi) {
								case 1:
									equipe2.add(new Tireur(p.getCoordBase(2),
											2, p));
									break;
								case 2:
									equipe2.add(new Piegeur(p.getCoordBase(2),
											2, p));
									break;
								case 3:
									equipe2.add(new Char(p.getCoordBase(2), 2,
											p));
									break;
								}
							}

							p.setEquipe1(equipe1);
							p.setEquipe2(equipe2);

							javax.swing.SwingUtilities
									.invokeLater(new Runnable() {
										public void run() {
											new IHM(p);
										}
									});
						}

					} else {
						f.dispose();
						p.setEquipe1(equipe1);
						p.setEquipe2(equipe2);

						javax.swing.SwingUtilities.invokeLater(new Runnable() {
							public void run() {
								new IHM(p);
							}
						});

					}
				}

				if (!choixDeux) {
					jLabel2.setText("Joueur 1 : Il vous reste "
							+ (p.nbRobots - robotChoisi)
							+ " robots a� choisir !");
				} else {
					jLabel2.setText("Joueur 2 : Il vous reste "
							+ (p.nbRobots - robotChoisi)
							+ " robots a� choisir !");
					jLabel2.setForeground(Color.GREEN);
				}
				jLabel3.setText("Tireur : " + tireurChoisi);

			}

		});

		jLabel4.addMouseListener(new MouseAdapter() {

			public void mouseEntered(MouseEvent evt) {
				jLabel4.setFont(new Font("Georgia", 1, 22));
			}

			public void mouseExited(MouseEvent evt) {
				jLabel4.setFont(new Font("Georgia", 0, 22));
			}
			
			public void mousePressed(MouseEvent e) {
				robotChoisi++;
				piegeurChoisi++;

				if (!choixDeux) {
					Piegeur piegeur = new Piegeur(p.getCoordBase(1), 1, p);
					equipe1.add(piegeur);
					p.getBase1().poserRobot(piegeur);

				} else {
					Piegeur piegeur = new Piegeur(p.getCoordBase(2), 2, p);
					equipe2.add(piegeur);
					p.getBase2().poserRobot(piegeur);
				}

				if (p.nbRobots - robotChoisi == 0) {
					if (!choixDeux) {
						if (p.modeChoisi == 1) {
							choixDeux = true;
							robotChoisi = 0;
							tireurChoisi = 0;
							piegeurChoisi = 0;
							charChoisi = 0;
							jLabel3.setText("Tireur : " + tireurChoisi);
							jLabel5.setText("Char : " + charChoisi);
						} else {
							f.dispose();

							// Constitution aleatoire de l'equipe 2.

							for (int i = 1; i <= p.nbRobots; i++) {
								int robotChoisi = new Random().nextInt(3) + 1;
								switch (robotChoisi) {
								case 1:
									equipe2.add(new Tireur(p.getCoordBase(2),
											2, p));
									break;
								case 2:
									equipe2.add(new Piegeur(p.getCoordBase(2),
											2, p));
									break;
								case 3:
									equipe2.add(new Char(p.getCoordBase(2), 2,
											p));
									break;
								}
							}

							p.setEquipe1(equipe1);
							p.setEquipe2(equipe2);

							javax.swing.SwingUtilities
									.invokeLater(new Runnable() {
										public void run() {
											new IHM(p);
										}
									});
						}
					} else {
						f.dispose();
						p.setEquipe1(equipe1);
						p.setEquipe2(equipe2);

						javax.swing.SwingUtilities.invokeLater(new Runnable() {
							public void run() {
								new IHM(p);
							}
						});

					}
				}

				if (!choixDeux) {
					jLabel2.setText("Joueur 1 : Il vous reste "
							+ (p.nbRobots - robotChoisi)
							+ " robots à choisir !");
				} else {
					jLabel2.setText("Joueur 2 : Il vous reste "
							+ (p.nbRobots - robotChoisi)
							+ " robots à choisir !");
					jLabel2.setForeground(Color.GREEN);
				}
				jLabel4.setText("Piegeur : " + piegeurChoisi);

			}

		});

		jLabel5.addMouseListener(new MouseAdapter() {

			public void mouseEntered(MouseEvent evt) {
				jLabel5.setFont(new Font("Georgia", 1, 22));
			}

			public void mouseExited(MouseEvent evt) {
				jLabel5.setFont(new Font("Georgia", 0, 22));
			}
			
			public void mousePressed(MouseEvent e) {
				robotChoisi++;
				charChoisi++;

				if (!choixDeux) {
					Char c = new Char(p.getCoordBase(1), 1, p);
					equipe1.add(c);
					p.getBase1().poserRobot(c);

				} else {
					Char c = new Char(p.getCoordBase(2), 2, p);
					equipe2.add(c);
					p.getBase2().poserRobot(c);

				}

				if (p.nbRobots - robotChoisi == 0) {
					if (!choixDeux) {
						if (p.modeChoisi == 1) {
							choixDeux = true;
							robotChoisi = 0;
							tireurChoisi = 0;
							piegeurChoisi = 0;
							charChoisi = 0;
							jLabel3.setText("Tireur : " + tireurChoisi);
							jLabel4.setText("Piegeur : " + piegeurChoisi);
						} else {
							f.dispose();

							// Constitution aleatoire de l'equipe 2.

							for (int i = 1; i <= p.nbRobots; i++) {
								int robotChoisi = new Random().nextInt(3) + 1;
								switch (robotChoisi) {
								case 1:
									equipe2.add(new Tireur(p.getCoordBase(2),
											2, p));
									break;
								case 2:
									equipe2.add(new Piegeur(p.getCoordBase(2),
											2, p));
									break;
								case 3:
									equipe2.add(new Char(p.getCoordBase(2), 2,
											p));
									break;
								}
							}

							p.setEquipe1(equipe1);
							p.setEquipe2(equipe2);

							javax.swing.SwingUtilities
									.invokeLater(new Runnable() {
										public void run() {
											new IHM(p);
										}
									});
						}
					} else {
						f.dispose();
						p.setEquipe1(equipe1);
						p.setEquipe2(equipe2);

						javax.swing.SwingUtilities.invokeLater(new Runnable() {
							public void run() {
								new IHM(p);
							}
						});

					}
				}

				if (!choixDeux) {
					jLabel2.setText("Joueur 1 : Il vous reste "
							+ (p.nbRobots - robotChoisi)
							+ " robots a choisir !");
				} else {
					jLabel2.setText("Joueur 2 : Il vous reste "
							+ (p.nbRobots - robotChoisi)
							+ " robots a� choisir !");
					jLabel2.setForeground(Color.GREEN);
				}
				jLabel5.setText("Char : " + charChoisi);

			}

		});

		final JLabel retour = new JLabel();

		retour.setIcon(new ImageIcon("fleche.png"));
		retour.addMouseListener(new MouseAdapter() {

			public void mouseEntered(MouseEvent evt) {
				retour.setIcon(new ImageIcon("fleche2.png"));
				retour.setBounds(10, 10, 55, 55);
			}

			public void mouseExited(MouseEvent evt) {
				retour.setIcon(new ImageIcon("fleche.png"));
				retour.setBounds(10, 10, 49, 49);
			}

			public void mousePressed(MouseEvent e) {
				f.dispose();
				p.suivant = false;

				javax.swing.SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						new Menu(p);
					}
				});
			}
		});
		retour.setBounds(10, 10, 49, 49);

		f.getContentPane().add(retour);
		f.getContentPane().add(jLabel2);
		f.getContentPane().add(jLabel3);
		f.getContentPane().add(jLabel4);
		f.getContentPane().add(jLabel5);
		f.getContentPane().add(jLabel1);

		this.setLayout(null);

		GroupLayout layout = new GroupLayout(f.getContentPane());
		f.getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(
				GroupLayout.Alignment.LEADING).addComponent(this,
				GroupLayout.DEFAULT_SIZE, 999, Short.MAX_VALUE));
		layout.setVerticalGroup(layout.createParallelGroup(
				GroupLayout.Alignment.LEADING).addComponent(this,
				GroupLayout.DEFAULT_SIZE, 728, Short.MAX_VALUE));

		f.pack();
		f.setLocationRelativeTo(null);
		f.setVisible(true);

	}
}
