package Interface;

import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import Plateau.Plateau;

@SuppressWarnings("serial")
public class EcranTitre extends JPanel {

	/**
	 * Constructeur pour l'�cran de titre qui permet d'acc�der au lancement du jeu ou aux r�gles
	 * @param p
	 */
	
	public EcranTitre(final Plateau p) {
		final JFrame f = new JFrame("VirtualWar");
		f.setResizable(false);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JLabel jLabel1 = new JLabel();
		JLabel jLabel2 = new JLabel();
		final JLabel jLabel3 = new JLabel();
		final JLabel jLabel4 = new JLabel();

		this.setLayout(null);

		jLabel1.setIcon(new ImageIcon("virtualwar.png"));
		jLabel1.setBounds(0, 0, 1000, 190);

		jLabel2.setIcon(new ImageIcon("simont.jpg"));
		jLabel2.setBounds(0, 0, 1000, 730);

		jLabel3.setFont(new Font("Georgia", 0, 48));
		jLabel3.setText("Jouer");
		jLabel3.addMouseListener(new MouseAdapter() {

			public void mouseEntered(MouseEvent evt) {
				jLabel3.setFont(new Font("Georgia", 1, 46));
				jLabel3.setBounds(150, 250, 175, 50);
			}

			public void mouseExited(MouseEvent evt) {
				jLabel3.setFont(new Font("Georgia", 0, 48));
				jLabel3.setBounds(150, 250, 160, 50);
			}

			public void mousePressed(MouseEvent evt) {
				f.dispose();

				javax.swing.SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						new ChoixMode(p);
					}
				});
			}
		});
		jLabel3.setBounds(150, 250, 160, 50);

		jLabel4.setFont(new Font("Georgia", 0, 48));
		jLabel4.setText("Regles");
		jLabel4.addMouseListener(new MouseAdapter() {

			public void mouseEntered(MouseEvent evt) {
				jLabel4.setFont(new Font("Georgia", 1, 46));
				jLabel4.setBounds(150, 325, 175, 60);
			}

			public void mouseExited(MouseEvent evt) {
				jLabel4.setFont(new Font("Georgia", 0, 48));
				jLabel4.setBounds(150, 325, 165, 60);
			}

			public void mousePressed(MouseEvent evt) {
				f.dispose();

				javax.swing.SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						new Regles(p);
					}
				});
			}
		});
		jLabel4.setBounds(150, 325, 165, 60);

		GroupLayout layout = new GroupLayout(f.getContentPane());
		f.getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(
				GroupLayout.Alignment.LEADING).addComponent(this,
				GroupLayout.DEFAULT_SIZE, 999, Short.MAX_VALUE));
		layout.setVerticalGroup(layout.createParallelGroup(
				GroupLayout.Alignment.LEADING).addComponent(this,
				GroupLayout.DEFAULT_SIZE, 728, Short.MAX_VALUE));

		this.add(jLabel3);
		this.add(jLabel4);
		this.add(jLabel1);
		this.add(jLabel2);

		f.pack();
		f.setLocationRelativeTo(null);
		f.setVisible(true);
	}
}
