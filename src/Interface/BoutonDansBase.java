package Interface;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JFrame;

import Robots.Robot;

@SuppressWarnings("serial")
public class BoutonDansBase extends JButton {

	/**
	 * Constructeur d'un Bouton dans la JFrame obtenu en cliquant sur un base. Un clic sur ce bouton permet de sélectionner un robot
	 * @param r
	 * @param f
	 */
	
	public BoutonDansBase(final Robot r, final JFrame f) {
		super(r.toString());

		this.addMouseListener(new MouseAdapter() {

			public void mousePressed(MouseEvent e) {
				for (Robot robot2 : r.getPlateau().getEquipe(r.getEquipe()))
					robot2.select = false;
				r.select = true;
				f.dispose();
			}

		});
	}

}
