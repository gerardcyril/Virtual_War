package Interface;

import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import Plateau.Plateau;

@SuppressWarnings("serial")
public class ChoixMode extends JPanel {

	/**
	 * Constructeur de la fen�tre pour choisir le mode de jeu
	 * @param p
	 */
	
	public ChoixMode(final Plateau p) {
		final JFrame f = new JFrame("VirtualWar");
		f.setResizable(false);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JLabel jLabel1 = new JLabel();
		final JLabel jLabel2 = new JLabel();
		final JLabel jLabel3 = new JLabel();
		final JLabel jLabel4 = new JLabel();
		final JLabel jLabel5 = new JLabel();
		
		jLabel1.setIcon(new ImageIcon("simont.jpg"));
		jLabel1.setBounds(0, 0, 1000, 730);

		jLabel2.setFont(new Font("Georgia", 0, 48));
		jLabel2.setText("Joueur VS Joueur");
		jLabel2.addMouseListener(new MouseAdapter() {

			public void mouseEntered(MouseEvent evt) {
				jLabel2.setFont(new Font("Georgia", 1, 46));
				jLabel2.setBounds(150, 100, 440, 50);
			}

			public void mouseExited(MouseEvent evt) {
				jLabel2.setFont(new Font("Georgia", 0, 48));
				jLabel2.setBounds(150, 100, 400, 50);
			}

			public void mousePressed(MouseEvent e) {
				f.dispose();
				p.modeChoisi = 1;
				
				javax.swing.SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						new Menu(p);
					}
				});
			}
		});
		jLabel2.setBounds(150, 100, 450, 50);
		
		jLabel3.setFont(new Font("Georgia", 0, 48));
		jLabel3.setText("Joueur VS IA");
		jLabel3.addMouseListener(new MouseAdapter() {

			public void mouseEntered(MouseEvent evt) {
				jLabel3.setFont(new Font("Georgia", 1, 46));
				jLabel3.setBounds(150, 200, 350, 50);
			}

			public void mouseExited(MouseEvent evt) {
				jLabel3.setFont(new Font("Georgia", 0, 48));
				jLabel3.setBounds(150, 200, 350, 50);
			}

			public void mousePressed(MouseEvent e) {
				f.dispose();
				p.modeChoisi = 2;
				
				javax.swing.SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						new Menu(p);
					}
				});
			}
		});
		jLabel3.setBounds(150, 200, 350, 50);

		
		jLabel4.setFont(new Font("Georgia", 0, 48));
		jLabel4.setText("IA VS IA");
		jLabel4.addMouseListener(new MouseAdapter() {

			public void mouseEntered(MouseEvent evt) {
				jLabel4.setFont(new Font("Georgia", 1, 46));
				jLabel4.setBounds(150, 300, 230, 50);
			}

			public void mouseExited(MouseEvent evt) {
				jLabel4.setFont(new Font("Georgia", 0, 48));
				jLabel4.setBounds(150, 300, 200, 50);
			}

			public void mousePressed(MouseEvent e) {
				f.dispose();
				p.modeChoisi = 3;
				
				javax.swing.SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						new Menu(p);
					}
				});
			}
		});
		jLabel4.setBounds(150, 300, 200, 50);		

		jLabel5.setIcon(new ImageIcon("fleche.png"));
		jLabel5.addMouseListener(new MouseAdapter() {

			public void mouseEntered(MouseEvent evt) {
				jLabel5.setIcon(new ImageIcon("fleche2.png"));
				jLabel5.setBounds(10,10, 55, 55);
			}

			public void mouseExited(MouseEvent evt) {
				jLabel5.setIcon(new ImageIcon("fleche.png"));
				jLabel5.setBounds(10, 10, 49, 49);
			}

			public void mousePressed(MouseEvent e) {
				f.dispose();
				
				javax.swing.SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						new EcranTitre(p);
					}
				});
			}
		});
		jLabel5.setBounds(10, 10, 49, 49);
		
		this.setLayout(null);

		GroupLayout layout = new GroupLayout(f.getContentPane());
		f.getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(
				GroupLayout.Alignment.LEADING).addComponent(this,
				GroupLayout.DEFAULT_SIZE, 999, Short.MAX_VALUE));
		layout.setVerticalGroup(layout.createParallelGroup(
				GroupLayout.Alignment.LEADING).addComponent(this,
				GroupLayout.DEFAULT_SIZE, 728, Short.MAX_VALUE));

		this.add(jLabel5);
		this.add(jLabel4);
		this.add(jLabel3);
		this.add(jLabel2);
		this.add(jLabel1);

		f.pack();
		f.setLocationRelativeTo(null);
		f.setVisible(true);
	}

}
