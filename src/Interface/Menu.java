package Interface;

import java.awt.Font;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import Plateau.Plateau;

@SuppressWarnings("serial")
public class Menu extends JPanel {

	/**
	 * Constructeur pour la fen�tre de s�lection des param�tres du plateau (taille, nbRobots, tauxObstacles)
	 * @param p
	 */
	
	public Menu(final Plateau p) {
		final JFrame f = new JFrame("VirtualWar");
		f.setResizable(false);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JLabel fond = new JLabel();
		fond.setIcon(new ImageIcon("simont.jpg"));
		fond.setBounds(0, 0, 1000, 730);
		
		JLabel label1 = new JLabel(
				"Choisir la largeur du plateau (comprise entre 3 et 20).");
		JLabel label2 = new JLabel(
				"Choisir la hauteur du plateau (comprise entre 3 et 20).");
		JLabel label3 = new JLabel("Taux d'obstacles (compris entre 0 et 100).");
		JLabel label4 = new JLabel(
				"Choisir un nombre de robots par equipe (de 1 a 5 unite(s)).");
		
		label1.setFont(new Font("Georgia", 0, 22));
		label2.setFont(new Font("Georgia", 0, 22));
		label3.setFont(new Font("Georgia", 0, 22));
		label4.setFont(new Font("Georgia", 0, 22));
		
		label1.setBounds(30, 30, 650, 40);
		label1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
		label2.setBounds(30, 130, 650, 40);
		label2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
		label3.setBounds(30, 230, 650, 40);
		label3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
		label4.setBounds(30, 330, 650, 40);
		label4.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);

		final JTextField f1 = new JTextField();
		final JTextField f2 = new JTextField();
		final JTextField f3 = new JTextField();
		final JTextField f4 = new JTextField();
		
		f1.setBounds(700, 30, 50, 40);
		f2.setBounds(700, 130, 50, 40);
		f3.setBounds(700, 230, 50, 40);
		f4.setBounds(700, 330, 50, 40);

		f1.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent e) {
				char c = e.getKeyChar();
				if ((c == KeyEvent.VK_ENTER)) {
					validation(p, f, f1, f2, f3, f4);
				} else if (!((c >= '0') && (c <= '9')
						|| (c == KeyEvent.VK_BACK_SPACE) || (c == KeyEvent.VK_DELETE))) {
					getToolkit().beep();
					e.consume();
				}
			}
		});
		f2.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent e) {
				char c = e.getKeyChar();
				if ((c == KeyEvent.VK_ENTER)) {
					validation(p, f, f1, f2, f3, f4);
				} else if (!((c >= '0') && (c <= '9')
						|| (c == KeyEvent.VK_BACK_SPACE) || (c == KeyEvent.VK_DELETE))) {
					getToolkit().beep();
					e.consume();
				}
			}
		});
		f3.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent e) {
				char c = e.getKeyChar();
				if ((c == KeyEvent.VK_ENTER)) {
					validation(p, f, f1, f2, f3, f4);
				} else if (!((c >= '0') && (c <= '9')
						|| (c == KeyEvent.VK_BACK_SPACE) || (c == KeyEvent.VK_DELETE))) {
					getToolkit().beep();
					e.consume();
				}
			}
		});
		f4.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent e) {
				char c = e.getKeyChar();
				if ((c == KeyEvent.VK_ENTER)) {
					validation(p, f, f1, f2, f3, f4);
				} else if (!((c >= '0') && (c <= '5')
						|| (c == KeyEvent.VK_BACK_SPACE) || (c == KeyEvent.VK_DELETE))) {
					getToolkit().beep();
					e.consume();
				}
			}
		});

		final JLabel label5 = new JLabel();
		
		label5.setIcon(new ImageIcon("fleche.png"));
		label5.addMouseListener(new MouseAdapter() {

			public void mouseEntered(MouseEvent evt) {
				label5.setIcon(new ImageIcon("fleche2.png"));
				label5.setBounds(10,10, 55, 55);
			}

			public void mouseExited(MouseEvent evt) {
				label5.setIcon(new ImageIcon("fleche.png"));
				label5.setBounds(10, 10, 49, 49);
			}

			public void mousePressed(MouseEvent e) {
				f.dispose();
				p.modeChoisi = 0;
				
				javax.swing.SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						new ChoixMode(p);
					}
				});
			}
		});
		label5.setBounds(10, 10, 49, 49);

		final JLabel next = new JLabel();
		
		next.setFont(new Font("Georgia", 0, 48));
		next.setText("Suivant");
		next.addMouseListener(new MouseAdapter() {

			public void mouseEntered(MouseEvent evt) {
				next.setFont(new Font("Georgia", 1, 46));
				next.setBounds(750, 180, 200, 50);
			}

			public void mouseExited(MouseEvent evt) {
				next.setFont(new Font("Georgia", 0, 48));
				next.setBounds(750, 180, 200, 50);
			}

			public void mousePressed(MouseEvent e) {
				validation(p, f, f1, f2, f3, f4);
			}
		});
		next.setBounds(775, 180, 200, 50);

		this.setLayout(null);

		GroupLayout layout = new GroupLayout(f.getContentPane());
		f.getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(
				GroupLayout.Alignment.LEADING).addComponent(this,
				GroupLayout.DEFAULT_SIZE, 999, Short.MAX_VALUE));
		layout.setVerticalGroup(layout.createParallelGroup(
				GroupLayout.Alignment.LEADING).addComponent(this,
				GroupLayout.DEFAULT_SIZE, 728, Short.MAX_VALUE));
		
		this.add(label1);
		this.add(f1);
		this.add(label2);
		this.add(f2);
		this.add(label3);
		this.add(f3);
		this.add(label4);
		this.add(f4);
		this.add(label5);
		this.add(next);
		this.add(fond);

		f.pack();
		f.setLocationRelativeTo(null);
		f.setVisible(true);
	}

	public void validation(final Plateau p, JFrame f, JTextField f1,
			JTextField f2, JTextField f3, JTextField f4) {
		try {
			int a = Integer.parseInt(f1.getText());
			int b = Integer.parseInt(f2.getText());
			int c = Integer.parseInt(f3.getText());
			int d = Integer.parseInt(f4.getText());

			if (a >= 3 && a <= 20 && b >= 3 && b <= 20 && c >= 0 && c <= 100
					&& d >= 1 && d <= 5) {
				p.suivant = true;
				p.setLargeur(a);
				p.setHauteur(b);
				p.setTauxObstacle(c);
				p.nbRobots = d;
				f.dispose();
				p.genererPlateau();
				p.genererObstacles();

				javax.swing.SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						new ChoixUnites(p);
					}
				});
			}
		} catch (Exception e2) {

		}
	}
}
