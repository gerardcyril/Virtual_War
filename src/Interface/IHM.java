package Interface;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Random;

import Action.Attaque;
import Action.Deplacement;
import Jeu.Bot;
import Jeu.Constantes;
import Plateau.Plateau;
import Robots.*;

import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.plaf.basic.BasicProgressBarUI;

@SuppressWarnings("serial")
public class IHM extends JPanel {

	private JFrame f = new JFrame("VirtualWar");
	public int choixRobot1 = 1;
	private boolean estMouvement = true;
	private boolean fini = false;
	private JLabel joueur = new JLabel();

	JLabel fond = new JLabel();

	/**
	 * Constructeur pour la fen�tre du jeu en lui-m�me. Il met en forme l'interface et disposent les �l�ments dans la fen�tre
	 * @param p
	 */
	
	public IHM(final Plateau p) {
		if (p.getLargeur() < 13 && p.getHauteur() < 13) {
			p.formatImages = 50;
		}

		int pLargeur = (p.getLargeur() + 2) * p.formatImages;
		int pHauteur = (p.getHauteur() + 2) * p.formatImages;

		f.setResizable(false);
		this.setLayout(null);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		p.setBounds((1000 - pLargeur) / 2, (730 - pHauteur) / 3, pLargeur,
				pHauteur);
		f.getContentPane().add(p);

		fond.setIcon(new ImageIcon("simont.jpg"));
		fond.setBounds(0, 0, 1000, 730);

		// Bouton pour choisir le d�placement � effectuer (D�placement ou
		// Attaque)

		final JLabel action = new JLabel();
		p.choixAction = 1;
		action.setIcon(new ImageIcon("move.jpg"));
		action.setBounds(5, 200, 130, 130);
		action.addMouseListener(new MouseAdapter() {

			public void mouseEntered(MouseEvent evt) {
				action.setBounds(5, 198, 130, 130);
			}

			public void mouseExited(MouseEvent evt) {
				action.setBounds(5, 200, 130, 130);
			}

			public void mousePressed(MouseEvent e) {
				if (estMouvement) {
					p.choixAction = 2;
					action.setIcon(new ImageIcon("attaque.jpg"));
					estMouvement = false;
				} else {
					p.choixAction = 1;
					action.setIcon(new ImageIcon("move.jpg"));
					estMouvement = true;
				}
			}
		});
		action.setFocusable(false);
		action.setToolTipText("Cliquez pour changer d'action !");
		this.add(action);

		joueur.setIcon(new ImageIcon("J1.png"));
		joueur.setBounds(5, 30, 130, 130);
		joueur.setFocusable(false);
		this.add(joueur);

		p.addMouseListener(new MouseAdapter() {

			public void mousePressed(MouseEvent e) {

				Point positionClic = e.getPoint();

				if (p.getBase1().r.contains(positionClic) && p.tour == 1) {
					JFrame fBase = new JFrame("Base 1");
					fBase.getContentPane().setLayout(
							new GridLayout(p.nbRobots, 1));
					fBase.setBackground(Color.WHITE);
					fBase.setResizable(false);
					fBase.setPreferredSize(new Dimension(100, 250));
					fBase.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

					for (Robot r : p.getEquipe(1)) {
						if (r.getCoordonnees().equals(p.getCoordBase(1)))
							fBase.add(new BoutonDansBase(r, fBase));
					}

					fBase.pack();
					fBase.setLocationRelativeTo(f);
					fBase.setVisible(true);
					f.addKeyListener(ajouterListenerAction(f,p));

				}

				else if (p.getBase2().r.contains(positionClic) && p.tour == -1) {
					JFrame fBase = new JFrame("Base 2");
					fBase.getContentPane().setLayout(
							new GridLayout(p.nbRobots, 1));
					fBase.setBackground(Color.WHITE);
					fBase.setResizable(false);
					fBase.setPreferredSize(new Dimension(100, 250));
					fBase.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

					for (Robot r : p.getEquipe(2)) {
						if (r.getCoordonnees().equals(p.getCoordBase(2)))
							fBase.add(new BoutonDansBase(r, fBase));
					}

					fBase.pack();
					fBase.setLocationRelativeTo(f);
					fBase.setVisible(true);
					f.addKeyListener(ajouterListenerAction(f,p));
				}

				else {

					for (Robot robot : p.getEquipe(1)) {
						if (!robot.getCoordonnees().equals(p.getCoordBase(1))
								&& p.tour == 1) {
							if (robot.r.contains(positionClic)) {
								for (Robot robot2 : p.getEquipe(1))
									robot2.select = false;
								if (e.getButton() == MouseEvent.BUTTON1) {
									robot.select = true;
									f.addKeyListener(ajouterListenerAction(f,p));
								}
								p.repaint();
							}
						}

					}

					for (Robot robot : p.getEquipe(2)) {
						if (!robot.getCoordonnees().equals(p.getCoordBase(2))
								&& p.tour == -1) {
							if (robot.r.contains(positionClic)) {
								for (Robot robot2 : p.getEquipe(2))
									robot2.select = false;
								if (e.getButton() == MouseEvent.BUTTON1) {
									robot.select = true;
									f.addKeyListener(ajouterListenerAction(f,p));
								}
								p.repaint();
							}
						}
					}
				}

			}

		});

		for (Robot r : p.getEquipe(1)) {
			r.barreVie.setMinimum(0);
			r.barreVie.setMaximum(r.getEnergieMax());
			r.barreVie.setBounds(5, 350 + 50 * p.getEquipe(1).indexOf(r), 130,
					30);
			r.barreVie.setValue(r.getEnergie());
			r.barreVie.setString(r.toString() + " : " + r.getEnergie());
			r.barreVie.setStringPainted(true);
			r.barreVie.setForeground(Color.YELLOW);
			r.barreVie.setUI(new BasicProgressBarUI() {

				protected Color getSelectionForeground() {
					return Color.black;
				}
			});
			this.add(r.barreVie);
			if(r instanceof Piegeur){
				r.barreMines.setMinimum(0);
				r.barreMines.setMaximum(Constantes.NB_MAX_MINES);
				r.barreMines.setBounds(5, 380 + 50 * p.getEquipe(1).indexOf(r), 130,
						15);
				r.barreMines.setValue(r.getNbMines());
				r.barreMines.setString(r.getNbMines() + " / " + Constantes.NB_MAX_MINES);
				r.barreMines.setStringPainted(true);
				r.barreMines.setForeground(Color.BLUE);
				this.add(r.barreMines);
			}
		}

		for (Robot r : p.getEquipe(2)) {
			r.barreVie.setMinimum(0);
			r.barreVie.setMaximum(r.getEnergieMax());
			r.barreVie.setBounds(865, 350 + 50 * p.getEquipe(2).indexOf(r),
					130, 30);
			r.barreVie.setValue(r.getEnergie());
			r.barreVie.setString(r.toString() + " : " + r.getEnergie());
			r.barreVie.setStringPainted(true);
			r.barreVie.setForeground(Color.GREEN);
			r.barreVie.setUI(new BasicProgressBarUI() {

				protected Color getSelectionForeground() {
					return Color.black;
				}
			});
			this.add(r.barreVie);
			if(r instanceof Piegeur){
				r.barreMines.setMinimum(0);
				r.barreMines.setMaximum(Constantes.NB_MAX_MINES);
				r.barreMines.setBounds(865, 380 + 50 * p.getEquipe(2).indexOf(r), 130,
						15);
				r.barreMines.setValue(r.getNbMines());
				r.barreMines.setString(r.getNbMines() + " / " + Constantes.NB_MAX_MINES);
				r.barreMines.setStringPainted(true);
				r.barreMines.setForeground(Color.BLUE);
				this.add(r.barreMines);
			}
		}

		this.add(fond);

		GroupLayout layout = new GroupLayout(f.getContentPane());
		f.getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(
				GroupLayout.Alignment.LEADING).addComponent(this,
				GroupLayout.DEFAULT_SIZE, 999, Short.MAX_VALUE));
		layout.setVerticalGroup(layout.createParallelGroup(
				GroupLayout.Alignment.LEADING).addComponent(this,
				GroupLayout.DEFAULT_SIZE, 728, Short.MAX_VALUE));

		f.pack();
		f.setLocationRelativeTo(null);
		f.setVisible(true);

	}

	// Fonction pour rajouter l'�coute des touches directionnelles

	/**
	 * Fonction pour rajouter l'�coute des touches directionnelles
	 * @param f
	 * @param p
	 * @return KeyAdapter	A mettre dans le listener du plateau apr�s avoir s�lectionn� un robot
	 */
	
	public KeyAdapter ajouterListenerAction(final JFrame f, final Plateau p) {

		KeyAdapter ka = new KeyAdapter() {

			public void keyPressed(KeyEvent e) {
				char c = e.getKeyChar();

				if (p.modeChoisi == 1) {
					if (p.choixAction == 1) {
						if (p.tour == 1) {
							for (Robot r : p.getEquipe(1)) {
								if ((r instanceof Tireur || r instanceof Piegeur)
										&& r.select) {
									switch (c) {
									case '1':
										new Deplacement(r,
												Constantes.BAS_GAUCHE).agit();
										break;
									case '2':
										new Deplacement(r, Constantes.BAS)
												.agit();
										break;
									case '3':
										new Deplacement(r,
												Constantes.BAS_DROITE).agit();
										break;
									case '4':
										new Deplacement(r, Constantes.GAUCHE)
												.agit();
										break;
									case '6':
										new Deplacement(r, Constantes.DROITE)
												.agit();
										break;
									case '7':
										new Deplacement(r,
												Constantes.HAUT_GAUCHE).agit();
										break;
									case '8':
										new Deplacement(r, Constantes.HAUT)
												.agit();
										break;
									case '9':
										new Deplacement(r,
												Constantes.HAUT_DROITE).agit();
										break;
									}
								} else if (r instanceof Char && r.select) {
									switch (c) {
									case '2':
										new Deplacement(r, Constantes.BAS)
												.agit();
										break;
									case '4':
										new Deplacement(r, Constantes.GAUCHE)
												.agit();
										break;
									case '6':
										new Deplacement(r, Constantes.DROITE)
												.agit();
										break;
									case '8':
										new Deplacement(r, Constantes.HAUT)
												.agit();
										break;
									}
								}
							}

						} else {
							for (Robot r : p.getEquipe(2)) {
								if ((r instanceof Tireur || r instanceof Piegeur)
										&& r.select) {
									switch (c) {
									case '1':
										new Deplacement(r,
												Constantes.BAS_GAUCHE).agit();
										break;
									case '2':
										new Deplacement(r, Constantes.BAS)
												.agit();
										break;
									case '3':
										new Deplacement(r,
												Constantes.BAS_DROITE).agit();
										break;
									case '4':
										new Deplacement(r, Constantes.GAUCHE)
												.agit();
										break;
									case '6':
										new Deplacement(r, Constantes.DROITE)
												.agit();
										break;
									case '7':
										new Deplacement(r,
												Constantes.HAUT_GAUCHE).agit();
										break;
									case '8':
										new Deplacement(r, Constantes.HAUT)
												.agit();
										break;
									case '9':
										new Deplacement(r,
												Constantes.HAUT_DROITE).agit();
										break;
									}
								} else if (r instanceof Char && r.select) {
									switch (c) {
									case '2':
										new Deplacement(r, Constantes.BAS)
												.agit();
										break;
									case '4':
										new Deplacement(r, Constantes.GAUCHE)
												.agit();
										break;
									case '6':
										new Deplacement(r, Constantes.DROITE)
												.agit();
										break;
									case '8':
										new Deplacement(r, Constantes.HAUT)
												.agit();
										break;
									}
								}
							}

						}
					} else if (p.choixAction == 2) {
						Attaque att = new Attaque();
						if (p.tour == 1) {
							for (Robot r : p.getEquipe(1)) {
								if (r instanceof Piegeur && r.select) {
									switch (c) {
									case '1':
										att = new Attaque(r,
												Constantes.BAS_GAUCHE);
										att.agit();
										break;
									case '2':
										att = new Attaque(r, Constantes.BAS);
										att.agit();
										break;
									case '3':
										att = new Attaque(r,
												Constantes.BAS_DROITE);
										att.agit();
										break;
									case '4':
										att = new Attaque(r, Constantes.GAUCHE);
										att.agit();
										break;
									case '6':
										att = new Attaque(r, Constantes.DROITE);
										att.agit();
										break;
									case '7':
										att = new Attaque(r,
												Constantes.HAUT_GAUCHE);
										att.agit();
										break;
									case '8':
										att = new Attaque(r, Constantes.HAUT);
										att.agit();
										break;
									case '9':
										att = new Attaque(r,
												Constantes.HAUT_DROITE);
										att.agit();
										break;
									}
								} else if ((r instanceof Char || r instanceof Tireur)
										&& r.select) {
									switch (c) {
									case '2':
										att = new Attaque(r, Constantes.BAS);
										att.agit();
										break;
									case '4':
										att = new Attaque(r, Constantes.GAUCHE);
										att.agit();
										break;
									case '6':
										att = new Attaque(r, Constantes.DROITE);
										att.agit();
										break;
									case '8':
										att = new Attaque(r, Constantes.HAUT);
										att.agit();
										break;
									}
								}
							}

						} else {
							for (Robot r : p.getEquipe(2)) {
								if (r instanceof Piegeur && r.select) {
									switch (c) {
									case '1':
										att = new Attaque(r,
												Constantes.BAS_GAUCHE);
										att.agit();
										break;
									case '2':
										att = new Attaque(r, Constantes.BAS);
										att.agit();
										break;
									case '3':
										att = new Attaque(r,
												Constantes.BAS_DROITE);
										att.agit();
										break;
									case '4':
										att = new Attaque(r, Constantes.GAUCHE);
										att.agit();
										break;
									case '6':
										att = new Attaque(r, Constantes.DROITE);
										att.agit();
										break;
									case '7':
										att = new Attaque(r,
												Constantes.HAUT_GAUCHE);
										att.agit();
										break;
									case '8':
										att = new Attaque(r, Constantes.HAUT);
										att.agit();
										break;
									case '9':
										att = new Attaque(r,
												Constantes.HAUT_DROITE);
										att.agit();
										break;
									}
								} else if ((r instanceof Char || r instanceof Tireur)
										&& r.select) {
									switch (c) {
									case '2':
										att = new Attaque(r, Constantes.BAS);
										att.agit();
										break;
									case '4':
										att = new Attaque(r, Constantes.GAUCHE);
										att.agit();
										break;
									case '6':
										att = new Attaque(r, Constantes.DROITE);
										att.agit();
										break;
									case '8':
										att = new Attaque(r, Constantes.HAUT);
										att.agit();
										break;
									}
								}
							}

						}

					}
				} else if (p.modeChoisi == 2) {
					if (p.tour == 1) {
						if (p.choixAction == 1) {
							for (Robot r : p.getEquipe(1)) {
								if ((r instanceof Tireur || r instanceof Piegeur)
										&& r.select) {
									switch (c) {
									case '1':
										new Deplacement(r,
												Constantes.BAS_GAUCHE).agit();
										break;
									case '2':
										new Deplacement(r, Constantes.BAS)
												.agit();
										break;
									case '3':
										new Deplacement(r,
												Constantes.BAS_DROITE).agit();
										break;
									case '4':
										new Deplacement(r, Constantes.GAUCHE)
												.agit();
										break;
									case '6':
										new Deplacement(r, Constantes.DROITE)
												.agit();
										break;
									case '7':
										new Deplacement(r,
												Constantes.HAUT_GAUCHE).agit();
										break;
									case '8':
										new Deplacement(r, Constantes.HAUT)
												.agit();
										break;
									case '9':
										new Deplacement(r,
												Constantes.HAUT_DROITE).agit();
										break;
									}
								} else if (r instanceof Char && r.select) {
									switch (c) {
									case '2':
										new Deplacement(r, Constantes.BAS)
												.agit();
										break;
									case '4':
										new Deplacement(r, Constantes.GAUCHE)
												.agit();
										break;
									case '6':
										new Deplacement(r, Constantes.DROITE)
												.agit();
										break;
									case '8':
										new Deplacement(r, Constantes.HAUT)
												.agit();
										break;
									}
								}
							}

						} else if (p.choixAction == 2) {
							Attaque att = new Attaque();
							if (p.tour == 1) {
								for (Robot r : p.getEquipe(1)) {
									if (r instanceof Piegeur && r.select) {
										switch (c) {
										case '1':
											att = new Attaque(r,
													Constantes.BAS_GAUCHE);
											att.agit();
											break;
										case '2':
											att = new Attaque(r, Constantes.BAS);
											att.agit();
											break;
										case '3':
											att = new Attaque(r,
													Constantes.BAS_DROITE);
											att.agit();
											break;
										case '4':
											att = new Attaque(r,
													Constantes.GAUCHE);
											att.agit();
											break;
										case '6':
											att = new Attaque(r,
													Constantes.DROITE);
											att.agit();
											break;
										case '7':
											att = new Attaque(r,
													Constantes.HAUT_GAUCHE);
											att.agit();
											break;
										case '8':
											att = new Attaque(r,
													Constantes.HAUT);
											att.agit();
											break;
										case '9':
											att = new Attaque(r,
													Constantes.HAUT_DROITE);
											att.agit();
											break;
										}
									} else if ((r instanceof Char || r instanceof Tireur)
											&& r.select) {
										switch (c) {
										case '2':
											att = new Attaque(r, Constantes.BAS);
											att.agit();
											break;
										case '4':
											att = new Attaque(r,
													Constantes.GAUCHE);
											att.agit();
											break;
										case '6':
											att = new Attaque(r,
													Constantes.DROITE);
											att.agit();
											break;
										case '8':
											att = new Attaque(r,
													Constantes.HAUT);
											att.agit();
											break;
										}
									}
								}

							}
						}
					} else {

						int cible = choixRobot1 - 1;
						int robot = 0;
						Bot bot = new Bot();
						System.out.println("Tour IA ");

						// IA pour le Char

						if (!p.getEquipe(2).isEmpty()
								&& p.getEquipe(2).get(robot) instanceof Char) {

							// Determine s'il doit se deplacer ou alors
							// attaquer

							if (bot.horsPortee(p.getEquipe(1), p.getEquipe(2),
									robot, cible)) {
								System.out.println("Char ne peut attaquer");

								// Determine la distance la plus courte
								// entre l'IA et la cible

								if (bot.cheminGD(p.getEquipe(1),
										p.getEquipe(2), robot, cible)) {
									System.out.println("Char Deplacement");
									bot.deplacementGD(p.getEquipe(1),
											p.getEquipe(2), robot, cible);
								}
								if (bot.cheminHB(p.getEquipe(1),
										p.getEquipe(2), robot, cible)) {
									System.out
											.println("Char Deplacement vertical");
									bot.deplacementHB(p.getEquipe(1),
											p.getEquipe(2), robot, cible);
								}
							}

							// L'IA attaque une fois a portee

							else {
								System.out.println("Char attaque");

								if ((p.getEquipe(1).get(cible).getY() - p
										.getEquipe(2).get(robot).getY()) > 0) {
									bot.attaqueBas(p, p.getEquipe(1),
											p.getEquipe(2), robot, cible);
								}

								else if ((p.getEquipe(1).get(cible).getY() - p
										.getEquipe(2).get(robot).getY()) < 0) {
									bot.attaqueHaut(p, p.getEquipe(1),
											p.getEquipe(2), robot, cible);
								}

								else if ((p.getEquipe(1).get(cible).getX() - p
										.getEquipe(2).get(robot).getX()) < 0) {
									bot.attaqueGauche(p, p.getEquipe(1),
											p.getEquipe(2), robot, cible);
								} else if ((p.getEquipe(1).get(cible).getX() - p
										.getEquipe(2).get(robot).getX()) > 0) {
									bot.attaqueDroite(p, p.getEquipe(1),
											p.getEquipe(2), robot, cible);
								}

							}

						}

						// IA pour le Tireur.

						if (!p.getEquipe(2).isEmpty()
								&& p.getEquipe(2).get(robot) instanceof Tireur) {

							if (p.getEquipe(1).isEmpty() == false) {

								// Determine s'il doit se deplacer ou alors
								// attaquer

								if (bot.horsPortee(p.getEquipe(1),
										p.getEquipe(2), robot, cible)) {
									System.out.println("Tireur dep");

									// Determine la distance la plus courte
									// entre l'IA et la cible

									if (bot.cheminGD(p.getEquipe(1),
											p.getEquipe(2), robot, cible)) {

										bot.deplacementGD(p.getEquipe(1),
												p.getEquipe(2), robot, cible);

									}
								}
								if (bot.cheminHB(p.getEquipe(1),
										p.getEquipe(2), robot, cible)) {

									bot.deplacementHB(p.getEquipe(1),
											p.getEquipe(2), robot, cible);
								}

								// L'IA attaque une fois a portee.

								else {
									System.out.println("Tireur attaque");

									if ((p.getEquipe(1).get(cible).getY() - p
											.getEquipe(2).get(robot).getY()) > 0) {

										bot.attaqueBas(p, p.getEquipe(1),
												p.getEquipe(2), robot, cible);
									}

									else if ((p.getEquipe(1).get(cible).getY() - p
											.getEquipe(2).get(robot).getY()) < 0) {

										bot.attaqueHaut(p, p.getEquipe(1),
												p.getEquipe(2), robot, cible);
									}

									else if ((p.getEquipe(1).get(cible).getX() - p
											.getEquipe(2).get(robot).getX()) < 0) {

										bot.attaqueGauche(p, p.getEquipe(1),
												p.getEquipe(2), robot, cible);

									}

									else if ((p.getEquipe(1).get(cible).getX() - p
											.getEquipe(2).get(robot).getX()) > 0) {

										bot.attaqueDroite(p, p.getEquipe(1),
												p.getEquipe(2), robot, cible);
									}

								}

							}
						}

						// IA pour le Piegeur.

						if (!p.getEquipe(2).isEmpty()
								&& p.getEquipe(2).get(robot) instanceof Piegeur) {

							// Determine s'il doit se deplacer ou alors
							// attaquer

							if (bot.horsPortee(p.getEquipe(1), p.getEquipe(2),
									robot, cible)) {

								System.out.println("Piegeur dep");

								// Determine la distance la plus courte
								// entre l'IA et la cible

								if (bot.cheminGD(p.getEquipe(1),
										p.getEquipe(2), robot, cible)) {

									bot.deplacementGD(p.getEquipe(1),
											p.getEquipe(2), robot, cible);
								}
								if (bot.cheminHB(p.getEquipe(1),
										p.getEquipe(2), robot, cible)) {

									bot.deplacementHB(p.getEquipe(1),
											p.getEquipe(2), robot, cible);
								}
							}

							// L'IA attaque une fois a portee.

							else {
								System.out.println("piegeur attaque");

								if ((p.getEquipe(1).get(cible).getY() - p
										.getEquipe(2).get(robot).getY()) > 0) {
									bot.attaqueBas(p, p.getEquipe(1),
											p.getEquipe(2), robot, cible);

								}

								else if ((p.getEquipe(1).get(cible).getY() - p
										.getEquipe(2).get(robot).getY()) < 0) {

									bot.attaqueHaut(p, p.getEquipe(1),
											p.getEquipe(2), robot, cible);
								}

								else if ((p.getEquipe(1).get(cible).getX() - p
										.getEquipe(2).get(robot).getX()) < 0) {
									bot.attaqueGauche(p, p.getEquipe(1),
											p.getEquipe(2), robot, cible);

								}

								else if ((p.getEquipe(1).get(cible).getX() - p
										.getEquipe(2).get(robot).getX()) > 0) {
									bot.attaqueDroite(p, p.getEquipe(1),
											p.getEquipe(2), robot, cible);
								}
							}

						}

						System.out.println("Fin tour IA");
					}
				} else if (p.modeChoisi == 3) {

					ArrayList<Robot> equipe3 = new ArrayList<Robot>(p.nbRobots);
					for (int i = 1; i <= p.nbRobots; i++) {
						int robotChoisi = new Random().nextInt(3) + 1;
						switch (robotChoisi) {
						case 1:
							equipe3.add(new Tireur(p.getCoordBase(1), 1, p));
							break;
						case 2:
							equipe3.add(new Piegeur(p.getCoordBase(1), 1, p));
							break;
						case 3:
							equipe3.add(new Char(p.getCoordBase(1), 1, p));
							break;
						}
					}

					p.setEquipe1(equipe3);

					ArrayList<Robot> equipe2 = new ArrayList<Robot>(p.nbRobots);
					for (int i = 1; i <= p.nbRobots; i++) {
						int robotChoisi = new Random().nextInt(3) + 1;
						switch (robotChoisi) {
						case 1:
							equipe2.add(new Tireur(p.getCoordBase(2), 2, p));
							break;
						case 2:
							equipe2.add(new Piegeur(p.getCoordBase(2), 2, p));
							break;
						case 3:
							equipe2.add(new Char(p.getCoordBase(2), 2, p));
							break;
						}
					}
					p.setEquipe2(equipe2);

					// Tour equipe 1.
					while (p.getEquipe(1).isEmpty() == false
							&& p.getEquipe(2).isEmpty() == false) {

						p.afficherPlateau(1);
						int cible = 0;

						int robot = 0;
						Bot bot = new Bot();
						int tour = 2;
						if (tour == 1) {

						} else {
							if (!equipe3.isEmpty()
									&& equipe3.get(robot) instanceof Char) {

								// Determine s'il doit se deplacer ou alors
								// attaquer

								if (bot.horsPortee(equipe2, equipe3, robot,
										cible)) {
									System.out.println("Char ne peut attaquer");

									// Determine la distance la plus courte
									// entre l'IA et la cible

									if (bot.cheminGD(equipe2, equipe3, robot,
											cible)) {
										System.out.println("Char Deplacement");
										bot.deplacementGD(equipe2, equipe3,
												robot, cible);
									}
									if (bot.cheminHB(equipe2, equipe3, robot,
											cible)) {
										System.out
												.println("Char Deplacement vertical");
										bot.deplacementHB(equipe2, equipe3,
												robot, cible);
									}
								}

								// L'IA attaque une fois a portee

								else {
									System.out.println("Char attaque");

									if ((equipe2.get(cible).getY() - equipe3
											.get(robot).getY()) > 0) {
										bot.attaqueBas(p, equipe2, equipe3,
												robot, cible);
									}

									else if ((equipe2.get(cible).getY() - equipe3
											.get(robot).getY()) < 0) {
										bot.attaqueHaut(p, equipe2, equipe3,
												robot, cible);
									}

									else if ((equipe2.get(cible).getX() - equipe3
											.get(robot).getX()) < 0) {
										bot.attaqueGauche(p, equipe2, equipe3,
												robot, cible);
									} else if ((equipe2.get(cible).getX() - equipe3
											.get(robot).getX()) > 0) {
										bot.attaqueDroite(p, equipe2, equipe3,
												robot, cible);
									}

								}
							}

							// IA pour le Tireur.

							if (!equipe3.isEmpty()
									&& equipe3.get(robot) instanceof Tireur) {

								// Determine s'il doit se deplacer ou alors
								// attaquer

								if (bot.horsPortee(equipe2, equipe3, robot,
										cible)) {
									System.out.println("Tireur dep");

									// Determine la distance la plus courte
									// entre l'IA et la cible

									if (bot.cheminGD(equipe2, equipe3, robot,
											cible)) {

										bot.deplacementGD(equipe2, equipe3,
												robot, cible);

									}
								}
								if (bot.cheminHB(equipe2, equipe3, robot, cible)) {

									bot.deplacementHB(equipe2, equipe3, robot,
											cible);
								}

								// L'IA attaque une fois a portee.

								else {
									System.out.println("Tireur attaque");

									if ((equipe2.get(cible).getY() - equipe3
											.get(robot).getY()) > 0) {

										bot.attaqueBas(p, equipe2, equipe3,
												robot, cible);
									}

									else if ((equipe2.get(cible).getY() - equipe3
											.get(robot).getY()) < 0) {

										bot.attaqueHaut(p, equipe2, equipe3,
												robot, cible);
									}

									else if ((equipe2.get(cible).getX() - equipe3
											.get(robot).getX()) < 0) {

										bot.attaqueGauche(p, equipe2, equipe3,
												robot, cible);

									}

									else if ((equipe2.get(cible).getX() - equipe3
											.get(robot).getX()) > 0) {

										bot.attaqueDroite(p, equipe2, equipe3,
												robot, cible);
									}

								}

							}

							// IA pour le Piegeur.

							if (!equipe3.isEmpty()
									&& equipe3.get(robot) instanceof Piegeur) {

								// Determine s'il doit se deplacer ou alors
								// attaquer

								if (bot.horsPortee(equipe2, equipe3, robot,
										cible)) {

									System.out.println("Piegeur dep");

									// Determine la distance la plus courte
									// entre l'IA et la cible

									if (bot.cheminGD(equipe2, equipe3, robot,
											cible)) {

										bot.deplacementGD(equipe2, equipe3,
												robot, cible);
									}
									if (bot.cheminHB(equipe2, equipe3, robot,
											cible)) {

										bot.deplacementHB(equipe2, equipe3,
												robot, cible);
									}
								}

								// L'IA attaque une fois a portee.

								else {
									System.out.println("piegeur attaque");

									if ((equipe2.get(cible).getY() - equipe3
											.get(robot).getY()) > 0) {
										bot.attaqueBas(p, equipe2, equipe3,
												robot, cible);

									}

									else if ((equipe2.get(cible).getY() - equipe3
											.get(robot).getY()) < 0) {

										bot.attaqueHaut(p, equipe2, equipe3,
												robot, cible);
									}

									else if ((equipe2.get(cible).getX() - equipe3
											.get(robot).getX()) < 0) {
										bot.attaqueGauche(p, equipe2, equipe3,
												robot, cible);

									}

									else if ((equipe2.get(cible).getX() - equipe3
											.get(robot).getX()) > 0) {
										bot.attaqueDroite(p, equipe2, equipe3,
												robot, cible);
									}
								}

							}
						}

						System.out.println("Fin tour IA");

						// Tour equipe 2.
						if (!equipe2.isEmpty()
								&& equipe2.get(robot) instanceof Char) {

							// Determine s'il doit se deplacer ou alors
							// attaquer

							if (bot.horsPortee(equipe3, equipe2, robot, cible)) {
								System.out.println("Char ne peut attaquer");

								// Determine la distance la plus courte
								// entre l'IA et la cible

								if (bot.cheminGD(equipe3, equipe2, robot, cible)) {
									System.out.println("Char Deplacement");
									bot.deplacementGD(equipe3, equipe2, robot,
											cible);
								}
								if (bot.cheminHB(equipe3, equipe2, robot, cible)) {
									System.out
											.println("Char Deplacement vertical");
									bot.deplacementHB(equipe3, equipe2, robot,
											cible);
								}
							}

							// L'IA attaque une fois a portee

							else {
								System.out.println("Char attaque");

								if ((equipe3.get(cible).getY() - equipe2.get(
										robot).getY()) > 0) {
									bot.attaqueBas(p, equipe3, equipe2, robot,
											cible);
								}

								else if ((equipe3.get(cible).getY() - equipe2
										.get(robot).getY()) < 0) {
									bot.attaqueHaut(p, equipe3, equipe2, robot,
											cible);
								}

								else if ((equipe3.get(cible).getX() - equipe2
										.get(robot).getX()) < 0) {
									bot.attaqueGauche(p, equipe3, equipe2,
											robot, cible);
								} else if ((equipe3.get(cible).getX() - equipe2
										.get(robot).getX()) > 0) {
									bot.attaqueDroite(p, equipe3, equipe2,
											robot, cible);
								}

							}

						}

						// IA pour le Tireur.

						if (!equipe2.isEmpty()
								&& equipe2.get(robot) instanceof Tireur) {

							if (p.getEquipe(2).isEmpty() == false) {

								// Determine s'il doit se deplacer ou alors
								// attaquer

								if (bot.horsPortee(equipe3, equipe2, robot,
										cible)) {
									System.out.println("Tireur dep");

									// Determine la distance la plus courte
									// entre l'IA et la cible

									if (bot.cheminGD(equipe3, equipe2, robot,
											cible)) {

										bot.deplacementGD(equipe3, equipe2,
												robot, cible);

									}
								}
								if (bot.cheminHB(equipe3, equipe2, robot, cible)) {

									bot.deplacementHB(equipe3, equipe2, robot,
											cible);
								}

								// L'IA attaque une fois a portee.

								else {
									System.out.println("Tireur attaque");

									if ((equipe3.get(cible).getY() - equipe2
											.get(robot).getY()) > 0) {

										bot.attaqueBas(p, equipe3, equipe2,
												robot, cible);
									}

									else if ((equipe3.get(cible).getY() - equipe2
											.get(robot).getY()) < 0) {

										bot.attaqueHaut(p, equipe3, equipe2,
												robot, cible);
									}

									else if ((equipe3.get(cible).getX() - equipe2
											.get(robot).getX()) < 0) {

										bot.attaqueGauche(p, equipe3, equipe2,
												robot, cible);

									}

									else if ((equipe3.get(cible).getX() - equipe2
											.get(robot).getX()) > 0) {

										bot.attaqueDroite(p, equipe3, equipe2,
												robot, cible);
									}

								}

							}
						}

						// IA pour le Piegeur.

						if (!equipe2.isEmpty()
								&& equipe2.get(robot) instanceof Piegeur) {

							// Determine s'il doit se deplacer ou alors
							// attaquer

							if (bot.horsPortee(equipe3, equipe2, robot, cible)) {

								System.out.println("Piegeur dep");

								// Determine la distance la plus courte
								// entre l'IA et la cible

								if (bot.cheminGD(equipe3, equipe2, robot, cible)) {

									bot.deplacementGD(equipe3, equipe2, robot,
											cible);
								}
								if (bot.cheminHB(equipe3, equipe2, robot, cible)) {

									bot.deplacementHB(equipe3, equipe2, robot,
											cible);
								}
							}

							// L'IA attaque une fois a portee.

							else {
								System.out.println("piegeur attaque");

								if ((equipe3.get(cible).getY() - equipe2.get(
										robot).getY()) > 0) {
									bot.attaqueBas(p, equipe3, equipe2, robot,
											cible);

								}

								else if ((equipe3.get(cible).getY() - equipe2
										.get(robot).getY()) < 0) {

									bot.attaqueHaut(p, equipe3, equipe2, robot,
											cible);
								}

								else if ((equipe3.get(cible).getX() - equipe2
										.get(robot).getX()) < 0) {
									bot.attaqueGauche(p, equipe3, equipe2,
											robot, cible);

								}

								else if ((equipe3.get(cible).getX() - equipe2
										.get(robot).getX()) > 0) {
									bot.attaqueDroite(p, equipe3, equipe2,
											robot, cible);
								}
							}
						}

						System.out.println("Fin tour IA");

					}

				}

				if (p.getEquipe(1).isEmpty() && !fini) {
					f.dispose();
					fini = true;
					javax.swing.SwingUtilities.invokeLater(new Runnable() {
						public void run() {
							new EcranWin(p);
						}

					});
					
				}

				else if (p.getEquipe(2).isEmpty() && !fini) {
					f.dispose();
					fini = true;
					javax.swing.SwingUtilities.invokeLater(new Runnable() {
						public void run() {
							new EcranWin(p);
						}

					});
				}

				for (Robot r : p.getEquipe(1)) {
					r.select = false;
				}

				for (Robot r : p.getEquipe(2)) {
					r.select = false;
				}

				if (p.tour == 1)
					joueur.setIcon(new ImageIcon("J1.png"));
				else
					joueur.setIcon(new ImageIcon("J2.png"));

				for (Robot r : p.getEquipe(1)) {
					if (r.getEnergie() <= 0)
						r.barreVie.setVisible(false);
					r.barreVie.setValue(r.getEnergie());
					r.barreVie.setBounds(5, 350 + 50 * p.getEquipe(1)
							.indexOf(r), 130, 30);
					if (r.getEnergie() < 10)
						r.barreVie.setForeground(Color.RED);
					r.barreVie.setString(r.toString() + " : " + r.getEnergie());
					if(r instanceof Piegeur){
						r.barreMines.setBounds(5, 380 + 50 * p.getEquipe(1).indexOf(r), 130,
								15);
						r.barreMines.setValue(r.getNbMines());
						r.barreMines.setString(r.getNbMines() + " / " + Constantes.NB_MAX_MINES);
					}
				}

				for (Robot r : p.getEquipe(2)) {
					if (r.getEnergie() <= 0)
						r.barreVie.setVisible(false);
					r.barreVie.setBounds(865, 350 + 50 * p.getEquipe(2)
							.indexOf(r), 130, 30);
					r.barreVie.setValue(r.getEnergie());
					r.barreVie.setString(r.toString() + " : " + r.getEnergie());
					if (r.getEnergie() < 10)
						r.barreVie.setForeground(Color.RED);
					if(r instanceof Piegeur){
						r.barreMines.setBounds(865, 380 + 50 * p.getEquipe(2).indexOf(r), 130,
								15);
						r.barreMines.setValue(r.getNbMines());
						r.barreMines.setString(r.getNbMines() + " / " + Constantes.NB_MAX_MINES);
					}
				}

				p.repaint();

			}

		};
		return ka;
	}
}
