package Interface;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import Plateau.Plateau;

@SuppressWarnings("serial")
public class EcranWin extends JPanel {

	/**
	 * Constructeur pour l'�cran de victoire, il clot�re une partie et permet de revenir � l'�cran titre
	 * @param p
	 */
	
	public EcranWin(final Plateau p) {
		final JFrame f = new JFrame("VirtualWar");
		f.setResizable(false);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		this.setLayout(null);

		JLabel fond = new JLabel();
		fond.setIcon(new ImageIcon("simont.jpg"));
		fond.setBounds(0, 0, 1000, 730);

		JLabel textWin = new JLabel();
		textWin.setFont(new Font("Georgia", 0, 55));
		textWin.setBounds(0, 75, 1000, 80);
		textWin.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		if (p.getEquipe(1).isEmpty()) {
			textWin.setForeground(Color.GREEN);
			textWin.setText("LE JOUEUR 2 GAGNE LA PARTIE !");
		} else if (p.getEquipe(2).isEmpty()) {
			textWin.setForeground(Color.YELLOW);
			textWin.setText("LE JOUEUR 1 GAGNE LA PARTIE !");
		}

		final JLabel retour = new JLabel("Retour au menu");
		retour.setFont(new Font("Georgia", 0, 40));
		retour.setBounds(0, 250, 1000, 45);
		retour.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		retour.addMouseListener(new MouseAdapter() {

			public void mouseEntered(MouseEvent evt) {
				retour.setFont(new Font("Georgia", 1, 42));
			}

			public void mouseExited(MouseEvent evt) {
				retour.setFont(new Font("Georgia", 0, 40));
			}

			public void mousePressed(MouseEvent e) {
				f.dispose();
				javax.swing.SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						new EcranTitre(p);
					}

				});
			};

		});
		
		f.add(textWin);
		f.add(retour);
		f.add(fond);
	
		
		GroupLayout layout = new GroupLayout(f.getContentPane());
		f.getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(
				GroupLayout.Alignment.LEADING).addComponent(this,
				GroupLayout.DEFAULT_SIZE, 999, Short.MAX_VALUE));
		layout.setVerticalGroup(layout.createParallelGroup(
				GroupLayout.Alignment.LEADING).addComponent(this,
				GroupLayout.DEFAULT_SIZE, 728, Short.MAX_VALUE));

		f.pack();
		f.setLocationRelativeTo(null);
		f.setVisible(true);

	}
}
