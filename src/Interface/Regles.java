package Interface;

import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import Plateau.Plateau;

@SuppressWarnings("serial")
public class Regles extends JPanel{

	/**
	 * Constructeur pour la fen�tre affichant les r�gles du jeu
	 * @param p
	 */
	
	public Regles(final Plateau p) {

		final JFrame f = new JFrame("VirtualWar");
		f.setResizable(false);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLayout(null);
		
		JLabel fond = new JLabel();
		fond.setIcon(new ImageIcon("simont.jpg"));
		fond.setBounds(0, 0, 1000, 730);
		
		JLabel rules = new JLabel();
		rules.setIcon(new ImageIcon("rules.png"));
		rules.setBounds(37, 200, 926, 382);
		
		JLabel titre = new JLabel("Regles du jeu");
		titre.setFont(new Font("Georgia", 1, 46));
		titre.setBounds(0, 100, 1000, 60);
		titre.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		
		final JLabel retour = new JLabel();
		
		retour.setIcon(new ImageIcon("fleche.png"));
		retour.addMouseListener(new MouseAdapter() {

			public void mouseEntered(MouseEvent evt) {
				retour.setIcon(new ImageIcon("fleche2.png"));
				retour.setBounds(10,10, 55, 55);
			}

			public void mouseExited(MouseEvent evt) {
				retour.setIcon(new ImageIcon("fleche.png"));
				retour.setBounds(10, 10, 49, 49);
			}

			public void mousePressed(MouseEvent e) {
				f.dispose();
				p.modeChoisi = 0;
				
				javax.swing.SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						new EcranTitre(p);
					}
				});
			}
		});
		retour.setBounds(10, 10, 49, 49);
		
		this.add(titre);
		this.add(retour);
		this.add(rules);
		this.add(fond);
		
		GroupLayout layout = new GroupLayout(f.getContentPane());
		f.getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(
				GroupLayout.Alignment.LEADING).addComponent(this,
				GroupLayout.DEFAULT_SIZE, 999, Short.MAX_VALUE));
		layout.setVerticalGroup(layout.createParallelGroup(
				GroupLayout.Alignment.LEADING).addComponent(this,
				GroupLayout.DEFAULT_SIZE, 728, Short.MAX_VALUE));
		
		f.pack();
		f.setLocationRelativeTo(null);
		f.setVisible(true);
	}

}
