package Jeu;

import java.util.ArrayList;
import java.util.Random;

import Action.Attaque;
import Action.Deplacement;
import Plateau.Plateau;
import Robots.Robot;

public class Bot {

	int nbRobots;

	public Bot() {

	}

	/**
	 * Determine si le Robot est a portee ou non pour attaquer
	 * 
	 * @param equipe1
	 * @param equipe2
	 * @param robot
	 * @param cible
	 * @return
	 */

	public boolean horsPortee(ArrayList<Robot> equipe1,
			ArrayList<Robot> equipe2, int robot, int cible) {

		if ((equipe2.get(robot).getPorteeAtt() < (Math.abs(equipe1.get(cible)
				.getX() - equipe2.get(robot).getX())))
				&& (equipe2.get(robot).getPorteeAtt() < (Math.abs(equipe1.get(
						cible).getY()
						- equipe2.get(robot).getY())))
				|| (equipe1.get(cible).getY() != equipe2.get(robot).getY() && equipe1
						.get(cible).getX() != equipe2.get(robot).getX())) {
			return true;
		}
		return false;

	}

	/**
	 * Retourne vrai si le chemin le plus court est gauche ou droite
	 * 
	 * @param equipe1
	 * @param equipe2
	 * @param robot
	 * @param cible
	 * @return
	 */
	public boolean cheminGD(ArrayList<Robot> equipe1, ArrayList<Robot> equipe2,
			int robot, int cible) {
		if ((Math.abs(equipe1.get(cible).getX() - equipe2.get(robot).getX())) < (Math
				.abs(equipe1.get(cible).getY() - equipe2.get(robot).getY()))) {
			return true;
		}
		return false;
	}

	/**
	 * Deplace l'ia a gauche ou a droite
	 * 
	 * @param equipe1
	 * @param equipe2
	 * @param robot
	 * @param cible
	 */
	public void deplacementGD(ArrayList<Robot> equipe1,
			ArrayList<Robot> equipe2, int robot, int cible) {
		if ((equipe1.get(cible).getX() - equipe2.get(robot).getX()) < 0) {
			new Deplacement(equipe2.get(robot), Constantes.GAUCHE).agit();
			System.out.println("Dep Gauche");
		}
		if ((equipe1.get(cible).getX() - equipe2.get(robot).getX()) > 0) {
			new Deplacement(equipe2.get(robot), Constantes.DROITE).agit();
			System.out.println("Dep Droite");
		}
	}

	/**
	 * Determine si le chemin le plus cour est haut ou bas
	 * 
	 * @param equipe1
	 * @param equipe2
	 * @param robot
	 * @param cible
	 * @return
	 */
	public boolean cheminHB(ArrayList<Robot> equipe1, ArrayList<Robot> equipe2,
			int robot, int cible) {
		if ((Math.abs(equipe1.get(cible).getX() - equipe2.get(robot).getX())) >= (Math
				.abs(equipe1.get(cible).getY() - equipe2.get(robot).getY()))) {
			return true;
		}
		return false;
	}

	/**
	 * Deplace l'ia vers haut ou bas
	 * @param equipe1
	 * @param equipe2
	 * @param robot
	 * @param cible
	 */
	public void deplacementHB(ArrayList<Robot> equipe1,
			ArrayList<Robot> equipe2, int robot, int cible) {
		if ((equipe1.get(cible).getY() - equipe2.get(robot).getY()) > 0) {
			new Deplacement(equipe2.get(robot), Constantes.BAS).agit();
			System.out.println("Dep Bas");
		}
		if ((equipe1.get(cible).getY() - equipe2.get(robot).getY()) < 0) {
			new Deplacement(equipe2.get(robot), Constantes.HAUT).agit();
			System.out.println("Dep Haut");
		}

	}

	/**
	 * attaque vers le bas ou se deplace si obstacle
	 * @param p
	 * @param equipe1
	 * @param equipe2
	 * @param robot
	 * @param cible
	 */
	public void attaqueBas(Plateau p, ArrayList<Robot> equipe1,
			ArrayList<Robot> equipe2, int robot, int cible) {
		for (int cel = equipe2.get(robot).getY(); cel <= equipe1.get(cible)
				.getY(); cel++) {
			if (p.getCellule(equipe2.get(robot).getX(), cel).estObstacle() == 0) {
				if (cel == equipe1.get(cible).getY()) {
					new Attaque(equipe2.get(robot), Constantes.BAS).agit();
					System.out.println("att BAS");
					break;
				}
			} else {
				int e;
				Random r = new Random();
				e = r.nextInt(2);
				System.out.println(e);
				if (e == 0) {
					new Deplacement(equipe2.get(robot), Constantes.DROITE)
							.agit();
				}
				if (e == 1) {
					new Deplacement(equipe2.get(robot), Constantes.GAUCHE)
							.agit();
				}
			}
		}

	}

	/**
	 * attaque vers le haut ou se deplace si obstacle
	 * @param p
	 * @param equipe1
	 * @param equipe2
	 * @param robot
	 * @param cible
	 */
	public void attaqueHaut(Plateau p, ArrayList<Robot> equipe1,
			ArrayList<Robot> equipe2, int robot, int cible) {
		for (int cel = equipe2.get(robot).getY(); cel >= equipe1.get(cible)
				.getY(); cel--) {
			if (p.getCellule(equipe2.get(robot).getX(), cel).estObstacle() == 0) {
				if (cel == equipe1.get(cible).getY()) {
					new Attaque(equipe2.get(robot), Constantes.HAUT).agit();
					System.out.println("att HAUT");
					break;
				}
			} else {
				int e;
				Random r = new Random();
				e = r.nextInt(2);
				System.out.println(e);
				if (e == 0) {
					new Deplacement(equipe2.get(robot), Constantes.DROITE)
							.agit();
				}
				if (e == 1) {
					new Deplacement(equipe2.get(robot), Constantes.GAUCHE)
							.agit();
				}
			}
		}
	}

	/**
	 * attaque vers la gauche ou se deplace si obstacle
	 * @param p
	 * @param equipe1
	 * @param equipe2
	 * @param robot
	 * @param cible
	 */
	public void attaqueGauche(Plateau p, ArrayList<Robot> equipe1,
			ArrayList<Robot> equipe2, int robot, int cible) {
		for (int cel = equipe2.get(robot).getX(); cel >= equipe1.get(cible)
				.getX(); cel--) {
			if (p.getCellule(cel, equipe2.get(robot).getY()).estObstacle() == 0) {
				if (cel == equipe1.get(cible).getX()) {
					new Attaque(equipe2.get(robot), Constantes.GAUCHE).agit();
					System.out.println("att GAUCHE");
					break;
				}
			} else {
				int e;
				Random r = new Random();
				e = r.nextInt(2);
				System.out.println(e);
				if (e == 0) {
					new Deplacement(equipe2.get(robot), Constantes.HAUT).agit();
				}
				if (e == 1) {
					new Deplacement(equipe2.get(robot), Constantes.BAS).agit();
				}
			}
		}
	}

	/**
	 * attaque vers la droite ou se deplace si obstacle
	 * @param p
	 * @param equipe1
	 * @param equipe2
	 * @param robot
	 * @param cible
	 */
	public void attaqueDroite(Plateau p, ArrayList<Robot> equipe1,
			ArrayList<Robot> equipe2, int robot, int cible) {
		for (int cel = equipe2.get(robot).getX(); cel <= equipe1.get(cible)
				.getX(); cel++) {
			if (p.getCellule(cel, equipe2.get(robot).getY()).estObstacle() == 0) {
				if (cel == equipe1.get(cible).getX()) {
					new Attaque(equipe2.get(robot), Constantes.DROITE).agit();
					System.out.println("att DROITE");
					break;
				}
			} else {
				int e;
				Random r = new Random();
				e = r.nextInt(2);
				System.out.println(e);
				if (e == 0) {
					new Deplacement(equipe2.get(robot), Constantes.HAUT).agit();
				}
				if (e == 1) {
					new Deplacement(equipe2.get(robot), Constantes.BAS).agit();
				}
			}
		}

	}
}
