package Jeu;

import Plateau.Coordonnees;

public class Constantes {
	
	// Directions
		
	public final static Coordonnees HAUT = new Coordonnees(0, -1);
	public final static Coordonnees BAS = new Coordonnees(0, 1);
	public final static Coordonnees GAUCHE = new Coordonnees(-1, 0);
	public final static Coordonnees DROITE = new Coordonnees(1, 0);

	public final static Coordonnees HAUT_GAUCHE = new Coordonnees(-1, -1);
	public final static Coordonnees HAUT_DROITE = new Coordonnees(1, -1);
	public final static Coordonnees BAS_GAUCHE = new Coordonnees(-1, 1);
	public final static Coordonnees BAS_DROITE = new Coordonnees(1, 1);

	// Constantes relatives au tireur

	public final static int PORTEE_ATT_TIREUR = 3;
	public final static int PORTEE_DEP_TIREUR = 1;
	public final static int DEGAT_TIREUR = 3;
	public final static int ENERGIE_MAX_TIREUR = 40;
	public final static int COUP_ATTAQUE_TIREUR = 2;
	public final static int COUP_DEPLACEMENT_TIREUR = 1;


	// Constantes relatives au char

	public final static int PORTEE_ATT_CHAR = 10;
	public final static int PORTEE_DEP_CHAR = 2;
	public final static int DEGAT_CHAR = 6;
	public final static int ENERGIE_MAX_CHAR = 60;
	public final static int COUP_ATTAQUE_CHAR = 1;
	public final static int COUP_DEPLACEMENT_CHAR = 5;

	// Constantes relatives au piegeur

	public final static int PORTEE_ATT_PIEGEUR = 1;
	public final static int PORTEE_DEP_PIEGEUR = 1;
	public final static int DEGAT_MINE = 2;
	public final static int ENERGIE_MAX_PIEGEUR = 50;
	public final static int COUP_ATTAQUE_PIEGEUR = 2;
	public final static int COUP_DEPLACEMENT_PIEGEUR = 2;
	public final static int NB_MAX_MINES = 10;
	
	

}
