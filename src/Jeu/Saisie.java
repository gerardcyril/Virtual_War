package Jeu;

import java.util.ArrayList;
import java.util.Scanner;

public class Saisie {

	public Saisie() {
	}

	/**
	 * Demande a l'utilisateur de rentrer un entier entre min et max. Tant que
	 * ce n'est pas le cas, l'utilisateur recommence l'operation. Renvoie cette
	 * entier a la fin.
	 * 
	 * @param min
	 * @param max
	 * @return int
	 */
	public int saisieEntier(int min, int max) {

		int a;
		Scanner sc = new Scanner(System.in);

		try {

			a = sc.nextInt();
			if (!(a >= min && a <= max)) {

				System.out.println("Saisie incorrect, recommencez (vous devez saisir un entier valide).");
				a = saisieEntier(min, max);

			}
		}

		catch (Exception e) {
			System.out.println("Saisie incorrect, recommencez (vous devez saisir un entier).");
			a = saisieEntier(min, max);
		}

		
		return a;
	}

	/**
	 * Demande a l'utilisateur de rentrer un entier compris dans la liste liste.
	 * Tant que ce n'est pas le cas, l'utilisateur recommence l'operation.
	 * Renvoie cette entier a la fin.
	 * 
	 * @param liste
	 * @return
	 */
	public int saisieEntier(ArrayList<Integer> liste) {

		int a;
		Scanner sc = new Scanner(System.in);

		try {

			a = sc.nextInt();
			if (!(liste.contains(a))) {

				System.out.println("Saisie incorrect, recommencez (vous devez saisir un entier valide).");
				a = saisieEntier(liste);

			}
		}

		catch (Exception e) {
			System.out.println("Saisie incorrect, recommencez (vous devez saisir un entier).");
			a = saisieEntier(liste);
		}

		
		return a;
	}
}
