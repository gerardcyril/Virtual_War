package Jeu;

import java.util.ArrayList;

import Interface.EcranTitre;
import Plateau.Plateau;

public class Main {

	public static void main(String[] args) {

		ArrayList<Integer> listeDirections1 = new ArrayList<Integer>();
		listeDirections1.add(1);
		listeDirections1.add(2);
		listeDirections1.add(3);
		listeDirections1.add(4);
		listeDirections1.add(6);
		listeDirections1.add(7);
		listeDirections1.add(8);
		listeDirections1.add(9);

		ArrayList<Integer> listeDirections2 = new ArrayList<Integer>();
		listeDirections2.add(2);
		listeDirections2.add(4);
		listeDirections2.add(6);
		listeDirections2.add(8);

		// Creation du plateau.

		final Plateau p = new Plateau();

		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new EcranTitre(p);
			}

		});

	}
}
