package Action;

import Jeu.Constantes;
import Plateau.Base;
import Plateau.Coordonnees;
import Robots.Char;
import Robots.Piegeur;
import Robots.Robot;
import Robots.Tireur;

public class Attaque extends Action {

	private Robot cible;
	private boolean cibleEliminee = false;

	public Attaque() {
		super();
	}

	/**
	 * Construit une instance d'Action de type Attaque.
	 * 
	 * @param robot
	 * @param direction
	 */
	public Attaque(Robot robot, Coordonnees direction) {
		super(robot, direction);
	}

	/**
	 * Permet au robot d'effectuer une attaque.
	 */
	public void agit() {

		if (getRobot().peutTirer()) {

			int i = 0;
			boolean attaqueTerminee = false;

			// Si la cellule de l'objectif est vide et que le robot est un
			// piegeur,
			// il pose une mine.
			if (getRobot() instanceof Piegeur
					&& getRobot().getPlateau().coordValide(getObjectif())
					&& getRobot().getPlateau().getCellule(getObjectif())
							.estVide()
					&& !(getRobot().getCoordonnees().equals(getRobot()
							.getPlateau().getCoordBase(getRobot().getEquipe())))
					&& !(getRobot().getPlateau().getCellule(getObjectif()) instanceof Base)) {

				getRobot().perdEnergie(Constantes.COUP_ATTAQUE_PIEGEUR);
				getRobot().getPlateau().getCellule(getObjectif())
						.poserMine(getRobot().getEquipe());
				getRobot().enleverMine();
				attaqueTerminee = true;
			}

			Coordonnees coordTestees = getObjectif().ajouterCoordonnees(
					getDirection().multiplierCoordonnees(i));

			while (attaqueTerminee == false
					&& i < getRobot().getPorteeAtt()
					&& getRobot().getPlateau().coordValide(coordTestees)
					&& getRobot().getPlateau().getCellule(coordTestees)
							.estObstacle() == 0
					&& getRobot().getPlateau().getCellule(coordTestees)
							.estBase() == 0
					&& !(getRobot().getCoordonnees().equals(getRobot()
							.getPlateau().getCoordBase(getRobot().getEquipe())))) {

				// Permet de determiner la premiere cellule sur laquelle se
				// trouve
				// un robot.
				coordTestees = getObjectif().ajouterCoordonnees(
						getDirection().multiplierCoordonnees(i));

				if (getRobot().getPlateau().coordValide(coordTestees)
						&& getRobot().getPlateau().getCellule(coordTestees)
								.getContenu() instanceof Robot
						&& getRobot().getPlateau().getCellule(coordTestees)
								.getContenu().getEquipe() != getRobot()
								.getEquipe()) {

					cible = getRobot().getPlateau().getCellule(coordTestees)
							.getContenu();

					if (getRobot() instanceof Tireur)
						getRobot().perdEnergie(Constantes.COUP_ATTAQUE_TIREUR);

					if (getRobot() instanceof Piegeur)
						getRobot().perdEnergie(Constantes.COUP_ATTAQUE_PIEGEUR);
					getRobot().enleverMine();

					if (getRobot() instanceof Char)
						getRobot().perdEnergie(Constantes.COUP_ATTAQUE_CHAR);

					// Permet de determiner les degats infliges au robot vise.
					getRobot().getPlateau().getCellule(coordTestees)
							.getContenu().subitDegats(getRobot());

					// Si le robot cible est detruit, il disparait du plateau de
					// jeu.
					if (getRobot().getPlateau().getCellule(coordTestees)
							.getContenu().getEnergie() <= 0) {
						getRobot().getPlateau().getCellule(coordTestees)
								.videCase();
						cibleEliminee = true;
					}
					attaqueTerminee = true;
				}
				if (cibleEliminee == true) {
					if (getRobot().getEquipe() == 1)
						getRobot().getPlateau().getEquipe(2).remove(getCible());

					if (getRobot().getEquipe() == 2)
						getRobot().getPlateau().getEquipe(1).remove(getCible());
				}
				i++;
			}

			if (attaqueTerminee) {
				getRobot().getPlateau().tour = -getRobot().getPlateau().tour;
				// Mise a jour de l'energie des robots sur leur
				// base.

				for (Robot r : getRobot().getPlateau().getEquipe(1)) {
					if (r.getCoordonnees().equals(
							getRobot().getPlateau().getCoordBase(1))
							&& r.getEnergie() < (r.getEnergieMax() - 1)) {
						r.setEnergie(r.getEnergie() + 2);
						if (r instanceof Piegeur)
							r.setNbMines(10);
						if (r.peutJouer() == false) {
							getRobot().getPlateau().getEquipe(1).remove(r);
							getRobot().getPlateau()
									.getCellule(r.getCoordonnees()).videCase();
						}
					} else if (r.getCoordonnees().equals(
							getRobot().getPlateau().getCoordBase(1))
							&& r.getEnergie() < (r.getEnergieMax())) {
						r.setEnergie(r.getEnergie() + 1);
						if (r instanceof Piegeur)
							r.setNbMines(10);
					}
				}

				for (Robot r : getRobot().getPlateau().getEquipe(2)) {
					if (r.getCoordonnees().equals(
							getRobot().getPlateau().getCoordBase(2))
							&& r.getEnergie() < (r.getEnergieMax() - 1)) {
						r.setEnergie(r.getEnergie() + 2);
						if (r instanceof Piegeur)
							r.setNbMines(10);
						if (r.peutJouer() == false) {
							getRobot().getPlateau().getEquipe(2).remove(r);
							getRobot().getPlateau()
									.getCellule(r.getCoordonnees()).videCase();
						}
					} else if (r.getCoordonnees().equals(
							getRobot().getPlateau().getCoordBase(2))
							&& r.getEnergie() < (r.getEnergieMax())) {
						r.setEnergie(r.getEnergie() + 1);
						if (r instanceof Piegeur)
							r.setNbMines(10);
					}
				}
			}

		}

	}

	/**
	 * @return true si la cible de l'attaque a ete eliminee.
	 */
	public boolean getCibleEliminee() {
		return cibleEliminee;
	}

	/**
	 * @return la cible de l'attaque.
	 */
	public Robot getCible() {
		return cible;
	}

}
