package Action;
import Plateau.Coordonnees;
import Robots.Robot;

public abstract class Action {

	private Robot robot;
	private Coordonnees direction;

	public Action() {

	}

	/**
	 * Constructeur de la classe action
	 * 
	 * @param robot
	 * @param direction
	 */
	public Action(Robot robot, Coordonnees direction) {
		this.robot = robot;
		this.direction = direction;
	}

	/**
	 * @return le robot courant
	 */
	public Robot getRobot() {
		return robot;
	}

	/**
	 * @return la coordonnee de l'objectif du robot
	 */
	public Coordonnees getObjectif() {
		return robot.getCoordonnees().ajouterCoordonnees(direction);
	}

	/**
	 * @return la direction courante
	 */
	public Coordonnees getDirection() {
		return direction;
	}

	/**
	 * Methode abstraite pour les actions
	 */
	public abstract void agit();

}
