package Action;

import Jeu.Constantes;
import Plateau.Coordonnees;
import Robots.Char;
import Robots.Piegeur;
import Robots.Robot;
import Robots.Tireur;

public class Deplacement extends Action {

	public Deplacement() {
	}

	/**
	 * Construit le deplacement d'un robot avec une direction.
	 * 
	 * @param robot
	 * @param direction
	 */
	public Deplacement(Robot robot, Coordonnees direction) {
		super(robot, direction);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see Action#agit()
	 */
	public void agit() {

		boolean deplacementFait = false;

		if (getRobot().getEnergie() > getRobot().getCoupDeplacement()) {

			if (getRobot().getPlateau().coordValide(getObjectif())
					&& getRobot().getPlateau().estLibre(getObjectif())
					&& ((getRobot().getPlateau().getCellule(getObjectif())
							.estBase() == getRobot().getEquipe() || getRobot()
							.getPlateau().getCellule(getObjectif()).estBase() == 0))) {

				if (getRobot().getPlateau().getCellule(getObjectif())
						.contientMine() != 0) {
					getRobot().subitMine();
					getRobot().getPlateau().getCellule(getObjectif())
							.retirerMine();
				}

				if (getRobot() instanceof Tireur)
					getRobot().perdEnergie(Constantes.COUP_DEPLACEMENT_TIREUR);

				if (getRobot() instanceof Piegeur)
					getRobot().perdEnergie(Constantes.COUP_DEPLACEMENT_PIEGEUR);

				if (getRobot() instanceof Char) {
					getRobot().perdEnergie(Constantes.COUP_DEPLACEMENT_CHAR);

					if (getRobot().getPlateau().coordValide(
							getObjectif().ajouterCoordonnees(getDirection()))
							&& getRobot().getPlateau().estLibre(
									getObjectif().ajouterCoordonnees(
											getDirection()))) {

						if ((getRobot()
								.getPlateau()
								.getCellule(
										getObjectif().ajouterCoordonnees(
												getDirection())).estBase() == getRobot()
								.getEquipe() || getRobot()
								.getPlateau()
								.getCellule(
										getObjectif().ajouterCoordonnees(
												getDirection())).estBase() == 0)) {

							getRobot().getPlateau().videCellule(
									getRobot().getCoordonnees());
							getRobot().setCoordonnees(
									getObjectif().ajouterCoordonnees(
											getDirection()));
							getRobot().getPlateau().poserRobot(getRobot());
							deplacementFait = true;

						}

					}

				}

				if (!deplacementFait) {

					getRobot().getPlateau().videCellule(
							getRobot().getCoordonnees());

					getRobot().setCoordonnees(getObjectif());
					getRobot().getPlateau().poserRobot(getRobot());
					deplacementFait = true;

				}

			}

			if (deplacementFait) {
				getRobot().getPlateau().tour = -getRobot().getPlateau().tour;
				// Mise a jour de l'energie des robots sur leur
				// base.

				for (Robot r : getRobot().getPlateau().getEquipe(1)) {
					if (r.getCoordonnees()
							.equals(getRobot().getPlateau().getCoordBase(1))
							&& r.getEnergie() < (r.getEnergieMax() - 1)) {
						r.setEnergie(r.getEnergie() + 2);
						if (r instanceof Piegeur)
							r.setNbMines(10);
						if (r.peutJouer() == false) {
							getRobot().getPlateau().getEquipe(1).remove(r);
							getRobot().getPlateau().getCellule(r.getCoordonnees())
									.videCase();
						}
					} else if (r.getCoordonnees().equals(
							getRobot().getPlateau().getCoordBase(1))
							&& r.getEnergie() < (r.getEnergieMax())) {
						r.setEnergie(r.getEnergie() + 1);
						if (r instanceof Piegeur)
							r.setNbMines(10);
					}
				}
				
				for (Robot r : getRobot().getPlateau().getEquipe(2)) {
					if (r.getCoordonnees()
							.equals(getRobot().getPlateau().getCoordBase(2))
							&& r.getEnergie() < (r.getEnergieMax() - 1)) {
						r.setEnergie(r.getEnergie() + 2);
						if (r instanceof Piegeur)
							r.setNbMines(10);
						if (r.peutJouer() == false) {
							getRobot().getPlateau().getEquipe(2).remove(r);
							getRobot().getPlateau().getCellule(r.getCoordonnees())
									.videCase();
						}
					} else if (r.getCoordonnees().equals(
							getRobot().getPlateau().getCoordBase(2))
							&& r.getEnergie() < (r.getEnergieMax())) {
						r.setEnergie(r.getEnergie() + 1);
						if (r instanceof Piegeur)
							r.setNbMines(10);
					}
				}
			}

		}
	}

}
