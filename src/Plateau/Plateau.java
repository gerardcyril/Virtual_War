package Plateau;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

import Robots.Char;
import Robots.Piegeur;
import Robots.Robot;
import Robots.Tireur;

@SuppressWarnings("serial")
public class Plateau extends JPanel {

	public Cellule[][] grille;
	private int largeur;
	private int hauteur;
	private Coordonnees coordBase1;
	private Coordonnees coordBase2;
	private Base base1;
	private Base base2;
	private ArrayList<Robot> equipe1 = new ArrayList<Robot>();
	private ArrayList<Robot> equipe2 = new ArrayList<Robot>();
	private int tauxObstacle;
	public boolean suivant = false;
	public int nbRobots;
	public int tour = 1;
	public int modeChoisi;
	public int formatImages = 30;

	public void setTauxObstacle(int tauxObstacle) {
		this.tauxObstacle = tauxObstacle;
	}

	public Plateau() {

	}

	public void genererPlateau() {
		grille = new Cellule[largeur][hauteur];
		for (int h = 0; h < hauteur; h++) {
			for (int l = 0; l < largeur; l++) {
				grille[l][h] = new Case(l, h);
			}
		}

		// Generation aleatoire des bases.

		Random r = new Random();
		if (r.nextInt(2) == 0) {
			base1 = new Base(0, 0, 1);
			grille[0][0] = base1;
			coordBase1 = new Coordonnees(0, 0);

			base2 = new Base(largeur - 1, hauteur - 1, 2);
			grille[largeur - 1][hauteur - 1] = base2;
			coordBase2 = new Coordonnees(largeur - 1, hauteur - 1);

		} else {
			base1 = new Base(0, hauteur - 1, 1);
			grille[0][hauteur - 1] = base1;
			coordBase1 = new Coordonnees(0, hauteur - 1);

			base2 = new Base(largeur - 1, 0, 2);
			grille[largeur - 1][0] = base2;
			coordBase2 = new Coordonnees(largeur - 1, 0);
		}
	}

	public void setLargeur(int largeur) {
		this.largeur = largeur;
	}

	public void setHauteur(int hauteur) {
		this.hauteur = hauteur;
	}

	/**
	 * Genere des obstacles selon un pourcentage du nombre de cellules de la
	 * grille avec au moins un chemin possible d'un base a une autre.
	 * 
	 * @param tauxObstacle
	 */
	public void genererObstacles() {
		Random r = new Random();

		int i = 0;
		int j = getCoordBase(1).getHauteur();
		while (i != getCoordBase(2).getLargeur()
				|| j != getCoordBase(2).getHauteur()) {
			getCellule(i, j).obstacle = 0;
			getCellule(i, j).dansChemin = true;

			if (r.nextInt(2) == 0 && i < getCoordBase(2).getLargeur()) {
				i = i + 1;
			} else {
				if (j < getCoordBase(2).getHauteur())
					j = j + 1;
				else if (j > getCoordBase(2).getHauteur())
					j = j - 1;
			}
		}
		getCellule(i, j).obstacle = 0;
		getCellule(i, j).dansChemin = true;

		for (int k = (getHauteur() * getLargeur()) * (100 - tauxObstacle) * 10; k < (getHauteur() * getLargeur()) * 1000; k = k + 1000) {
			int l = r.nextInt(getLargeur());
			int h = r.nextInt(getHauteur());
			if (getCellule(l, h).obstacle == 0 && !getCellule(l, h).dansChemin)
				getCellule(l, h).obstacle = 1;
			else
				k = k - 999;
		}
	}

	/**
	 * Affiche les informations des deux equipes (leurs robots avec leur energie
	 * et leur nombre de points d'energie.
	 */
	public void afficherInfos() {

		String res = "\n Equipe 1 : \t\t\t\t Equipe 2 : \n";
		int i = 0;
		while (i < equipe1.size() || i < equipe2.size()) {
			if (i < equipe1.size()) {
				res = res + " " + equipe1.get(i).toString() + " ( Energie : "
						+ equipe1.get(i).getEnergie() + " ) ";
				if (equipe1.get(i) instanceof Piegeur) {
					res = res + "( Mines : " + equipe1.get(i).getNbMines()
							+ " ) ";
				} else {
					res = res + "\t\t";
				}
			} else {
				res = res + "\t\t\t\t";
			}
			if (i < equipe2.size()) {
				res = res + "\t " + equipe2.get(i).toString() + " ( Energie : "
						+ equipe2.get(i).getEnergie() + " ) ";
				if (equipe2.get(i) instanceof Piegeur)
					res = res + "( Mines : " + equipe2.get(i).getNbMines()
							+ " ) ";
				res = res + "\n";
			}
			i++;
		}

		System.out.println(res + "\n");

	}

	/**
	 * Affiche le plateau apres avoir rafraichi l'ecran.
	 */

	public void afficherPlateau(int equipe) {

		try {
			Runtime.getRuntime().exec("cls");
		} catch (Exception e) {
			for (int i = 0; i < 50; i++)
				System.out.println();
		}

		for (int h = 0; h < hauteur; h++) {
			String res = "";
			for (int l = 0; l < largeur; l++) {
				res = res + " | " + this.grille[l][h].toString(equipe) + " | ";
				if (l == largeur - 1) {
					System.out.println(res);
				}
			}
		}

		afficherInfos();
		repaint();
	}

	/**
	 * Recupere la cellule du plateau a la position largeur, hauteur.
	 * 
	 * @param largeur
	 * @param hauteur
	 * @return
	 */
	public Cellule getCellule(int largeur, int hauteur) {
		return grille[largeur][hauteur];
	}

	/**
	 * Recupere la cellule du plateau de Coordonnees coord.
	 * 
	 * @param coord
	 * @return
	 */
	public Cellule getCellule(Coordonnees coord) {
		return grille[coord.getLargeur()][coord.getHauteur()];
	}

	/**
	 * Pose le robot passe en parametre sur la case sur laquelle il est suppose
	 * etre, soit la case qui a les memes coordonnees que lui.
	 * 
	 * @param robot
	 */
	public void poserRobot(Robot robot) {
		grille[robot.getCoordonnees().getLargeur()][robot.getCoordonnees()
				.getHauteur()].poserRobot(robot);
	}

	/**
	 * Pose le robot passe en parametre sur la cellule de Coordonnees coord.
	 * 
	 * @param robot
	 * @param coord
	 */
	public void poserRobot(Robot robot, Coordonnees coord) {
		getCellule(coord).poserRobot(robot);
	}

	public Cellule[][] getGrille() {
		return grille;
	}

	public int getLargeur() {
		return largeur;
	}

	public int getHauteur() {
		return hauteur;
	}

	/**
	 * Verifie si les Coordonnees coord passees en parametres sont bien sur le
	 * plateau.
	 * 
	 * @param coord
	 * @return boolean
	 */
	public boolean coordValide(Coordonnees coord) {
		return (coord.getLargeur() < this.getLargeur()
				&& coord.getLargeur() >= 0
				&& coord.getHauteur() < this.getHauteur() && coord.getHauteur() >= 0);
	}

	/**
	 * Verifie si la cellule de Coordonnees coord est libre (ni robot, ni
	 * obstacle).
	 * 
	 * @param coord
	 * @return
	 */
	public boolean estLibre(Coordonnees coord) {
		return (this.getCellule(coord).estObstacle() == 0 && this.getCellule(
				coord).getContenu() == null);
	}

	/**
	 * Vide le contenu de la cellule de Coordonnees coord.
	 * 
	 * @param coord
	 */
	public void videCellule(Coordonnees coord) {
		this.getCellule(coord).videCase();
	}

	/**
	 * Recupere les Coordonnees de l'equipe rentree en parametre.
	 * 
	 * @param equipe
	 * @return Coordonnees
	 */
	public Coordonnees getCoordBase(int equipe) {
		if (equipe == 1)
			return coordBase1;
		else
			return coordBase2;
	}

	/**
	 * Recupere la liste de robot de l'equipe rentree en parametre.
	 * 
	 * @param equipe
	 * @return ArrayList<Robot>
	 */
	public ArrayList<Robot> getEquipe(int equipe) {
		if (equipe == 1)
			return equipe1;
		else
			return equipe2;
	}

	public void setEquipe1(ArrayList<Robot> equipe1) {
		this.equipe1 = equipe1;
	}

	public void setEquipe2(ArrayList<Robot> equipe2) {
		this.equipe2 = equipe2;
	}

	// Bool�ens (comme des bool�ens) qui donne l'�tape � effectuer.
	public int choixAction = 0;
	public int choixRobot = 0;
	public int choixDirection = 0;

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		setBackground(Color.LIGHT_GRAY);

		String path = "images/";
		if (formatImages != 30)
			path = "images50/";

		for (int h = 0; h < hauteur; h++) {
			for (int l = 0; l < largeur; l++) {

				ImageIcon img = new ImageIcon(path + "vide.jpg");
				

				if (grille[l][h].contientMine() != 0 && grille[l][h].contientMine() == 2 && tour == -1)
					img = new ImageIcon(path + "mine.jpg");
				if (grille[l][h].contientMine() != 0 && grille[l][h].contientMine() == 1 && tour == 1)
					img = new ImageIcon(path + "mine.jpg");
				else if (grille[l][h].estObstacle() != 0)
					img = new ImageIcon(path + "obstacle.jpg");
				else if (grille[l][h].estBase() == 1) {
					img = new ImageIcon(path + "basej.jpg");
					base1.r = new Rectangle((grille[l][h].getCoordonnees()
							.getLargeur() + 1) * formatImages, (grille[l][h]
							.getCoordonnees().getHauteur() + 1) * formatImages,
							formatImages, formatImages);
				} else if (grille[l][h].estBase() == 2) {
					img = new ImageIcon(path + "basev.jpg");
					base2.r = new Rectangle((grille[l][h].getCoordonnees()
							.getLargeur() + 1) * formatImages, (grille[l][h]
							.getCoordonnees().getHauteur() + 1) * formatImages,
							formatImages, formatImages);
				} else if (grille[l][h].estBase() == 0) {
					if (grille[l][h].getContenu() != null) {
						grille[l][h].getContenu().r = new Rectangle(
								(grille[l][h].getCoordonnees().getLargeur() + 1)
										* formatImages, (grille[l][h]
										.getCoordonnees().getHauteur() + 1)
										* formatImages, formatImages,
								formatImages);

						if (grille[l][h].getContenu().getEquipe() == 1) {
							if (grille[l][h].getContenu().select) {
								if (grille[l][h].getContenu() instanceof Tireur)
									img = new ImageIcon(path
											+ "tireurjselec.jpg");
								else if (grille[l][h].getContenu() instanceof Piegeur)
									img = new ImageIcon(path
											+ "piegeurjselec.jpg");
								else if (grille[l][h].getContenu() instanceof Char)
									img = new ImageIcon(path
											+ "charjselec.jpg");
							} else {
								if (grille[l][h].getContenu() instanceof Tireur)
									img = new ImageIcon(path + "tireurj.jpg");
								else if (grille[l][h].getContenu() instanceof Piegeur)
									img = new ImageIcon(path + "piegeurj.jpg");
								else if (grille[l][h].getContenu() instanceof Char)
									img = new ImageIcon(path + "charj.jpg");
							}

						} else {
							if (grille[l][h].getContenu().select) {
								if (grille[l][h].getContenu() instanceof Tireur)
									img = new ImageIcon(path
											+ "tireurvselec.jpg");
								else if (grille[l][h].getContenu() instanceof Piegeur)
									img = new ImageIcon(path
											+ "piegeurvselec.jpg");
								else if (grille[l][h].getContenu() instanceof Char)
									img = new ImageIcon(path
											+ "charvselec.jpg");
							} else {
								if (grille[l][h].getContenu() instanceof Tireur)
									img = new ImageIcon(path + "tireurv.jpg");
								else if (grille[l][h].getContenu() instanceof Piegeur)
									img = new ImageIcon(path + "piegeurv.jpg");
								else if (grille[l][h].getContenu() instanceof Char)
									img = new ImageIcon(path + "charv.jpg");
							}
						}
					}
				}

				Image b = img.getImage();
				g.drawImage(b, (grille[l][h].getCoordonnees().getLargeur() + 1)
						* formatImages, (grille[l][h].getCoordonnees()
						.getHauteur() + 1) * formatImages, null);

				g.setColor(Color.BLACK);
				g.drawRect((grille[l][h].getCoordonnees().getLargeur() + 1)
						* formatImages, (grille[l][h].getCoordonnees()
						.getHauteur() + 1) * formatImages, formatImages,
						formatImages);

			}
		}

	}

	public Base getBase1() {
		return base1;
	}

	public Base getBase2() {
		return base2;
	}
}