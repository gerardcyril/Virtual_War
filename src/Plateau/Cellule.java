package Plateau;
import Robots.Robot;

public abstract class Cellule {

	public boolean dansChemin = false;

	protected int mine = 0;
	protected int base = 0;
	protected int obstacle = 0;
	protected Robot robot = null;
	private Coordonnees coord = new Coordonnees();

	public Cellule() {
	}

	/**
	 * D�finie les coordonnees de la cellule
	 * 
	 * @param largeur
	 * @param hauteur
	 */
	public Cellule(int largeur, int hauteur) {
		this.coord = new Coordonnees(largeur, hauteur);
	}

	/**
	 * Verifie si la cellule contient une mine.
	 * 
	 * @return le numero de l'equipe qui a pose la mine, 0 si il n'y a pas de
	 *         mine.
	 */
	public int contientMine() {
		return mine;
	}

	/**
	 * pose une mine
	 * 
	 * @param equipe
	 */
	public void poserMine(int equipe) {
		mine = equipe;
	}

	/**
	 * Retire une mine de la cellule en fixant son attribut � 0.
	 */
	public void retirerMine() {
		mine = 0;
	}

	/**
	 * Verifie si la cellule est une base.
	 * 
	 * @return le numero le l'equipe si c'est une base, 0 sinon.
	 */
	public int estBase() {
		return base;
	}

	/**
	 * @return un boolean si la cellule est vide
	 */
	public boolean estVide() {
		return (mine == 0 && obstacle == 0 && robot == null);
	}

	/**
	 * Donne les coordonnees de la cellule.
	 * 
	 * @return Coordonnees
	 */
	public Coordonnees getCoordonnees() {
		return coord;
	}

	/**
	 * Verifie si la cellule contient un obstacle.
	 * 
	 * @return int
	 */
	public int estObstacle() {
		return obstacle;
	}

	/**
	 * Retourne le robot contenu dans la cellule, null sinon.
	 * 
	 * @return Robot
	 */
	public Robot getContenu() {
		return robot;
	}

	public String toString(int equipe) {
		if (contientMine() == equipe)
			return "M ";
		if (estBase() == 1)
			return "b ";
		if (estBase() == 2)
			return "B ";
		if (estObstacle() != 0)
			return "O ";
		if (estVide())
			return "..";
		if (getContenu() != null) {
			return robot.toString();
		} else {
			return "..";
		}
	}

	/**
	 * Pose le Robot robot sur la cellule courante.
	 * 
	 * @param robot
	 */
	public abstract void poserRobot(Robot robot);

	/**
	 * Enleve les potentiels mines et/ou robot d'une cellule.
	 */
	public abstract void videCase();

}
