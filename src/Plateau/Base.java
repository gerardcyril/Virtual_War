package Plateau;

import java.awt.Rectangle;
import java.util.ArrayList;

import Robots.Robot;

public class Base extends Cellule {

	public Rectangle r;

	public ArrayList<Robot> listeRobots = new ArrayList<Robot>(5);

	/**
	 * Construit une nouvelle base de Coordonnees largeur, hauteur, et
	 * appartenant � l'�quipe equipe.
	 * 
	 * @param largeur
	 * @param hauteur
	 * @param equipe
	 */
	public Base(int largeur, int hauteur, int equipe) {
		super(largeur, hauteur);
		this.base = equipe;
	}

	/**
	 * Permet d'ajouter le Robot robot � la liste des Robots de la Base
	 * uniquement si robot est de la m�me �quipe que la Base.
	 */
	public void poserRobot(Robot robot) {
		if (robot.getEquipe() == this.base)
			listeRobots.add(robot);
	}

	/**
	 * Vide la base.
	 */
	public void videCase() {
		listeRobots = new ArrayList<Robot>(5);
	}

}
