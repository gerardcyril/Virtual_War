package Plateau;
public class Coordonnees {

	private int largeur;
	private int hauteur;

	public Coordonnees() {
	}

	/**
	 * Construit un nouvel objet Coordonnees de largeur largeur et de hauteur
	 * hauteur.
	 * 
	 * @param largeur
	 * @param hauteur
	 */
	public Coordonnees(int largeur, int hauteur) {
		this.largeur = largeur;
		this.hauteur = hauteur;
	}

	public int getLargeur() {
		return largeur;
	}

	public void setLargeur(int largeur) {
		this.largeur = largeur;
	}

	public int getHauteur() {
		return hauteur;
	}

	public void setHauteur(int hauteur) {
		this.hauteur = hauteur;
	}

	public String toString() {
		return new String("" + largeur + "," + hauteur);
	}

	/**
	 * Retourne vrai si l'objet Coordonnees courant est egal a l'objet coord
	 * passe en parametre.
	 * 
	 * @param coord
	 * @return boolean
	 */
	public boolean equals(Coordonnees coord) {
		return (this.getLargeur() == coord.getLargeur() && this.getHauteur() == coord.getHauteur());
	}

	/**
	 * Retourne le resultat obtenu par l'addition des Coordonnees courantes et
	 * des Coordonnees coord passees en parametre.
	 * 
	 * @param coord
	 */
	public Coordonnees ajouterCoordonnees(Coordonnees coord) {
		return new Coordonnees(this.getLargeur() + coord.getLargeur(), this.getHauteur() + coord.getHauteur());

	}

	/**
	 * Retourne le resultat de la multiplication de l'objet courant par un
	 * entier i.
	 * 
	 * @param i
	 * @return coord
	 */
	public Coordonnees multiplierCoordonnees(int i) {
		return new Coordonnees(this.largeur * i, this.hauteur * i);
	}

}
