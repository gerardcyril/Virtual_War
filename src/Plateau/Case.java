package Plateau;
import Robots.Robot;

public class Case extends Cellule {

	public Case() {
	}

	/**
	 * Construit une case issue d'une cellule.
	 * 
	 * @param largeur
	 * @param hauteur
	 */
	public Case(int largeur, int hauteur) {
		super(largeur, hauteur);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see Cellule#poserRobot(Robot)
	 */
	public void poserRobot(Robot robot) {
		if (this.mine != 0)
			robot.subitMine();
		this.robot = robot;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see Cellule#videCase()
	 */
	public void videCase() {
		robot = null;
		mine = 0;
	}
}
