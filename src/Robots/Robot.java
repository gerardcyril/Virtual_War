package Robots;

import java.awt.Rectangle;

import javax.swing.JPanel;
import javax.swing.JProgressBar;

import Jeu.Constantes;
import Plateau.Coordonnees;
import Plateau.Plateau;

@SuppressWarnings("serial")
public abstract class Robot extends JPanel {

	private int energie;
	private int energieMax;
	private int equipe;
	private int porteeAtt;
	private int porteeDep;
	private int idUnite;
	private int nbMines = 0;
	private Plateau plateau;
	private Coordonnees coord = new Coordonnees();
	public Rectangle r;
	public boolean select = false;

	public JProgressBar barreVie = new JProgressBar();
	public JProgressBar barreMines = new JProgressBar();

	/**
	 * Construit un robot avec une coordonnee, une equipe et un plateau.
	 * 
	 * @param coord
	 * @param equipe
	 * @param plateau
	 */
	public Robot(Coordonnees coord, int equipe, Plateau plateau) {
		this.coord = coord;
		this.equipe = equipe;
		this.plateau = plateau;
	}

	/**
	 * Construit un robot avec une hauteur, une largeur, une equipe et un
	 * plateau.
	 * 
	 * @param largeur
	 * @param hauteur
	 * @param equipe
	 * @param plateau
	 */
	public Robot(int largeur, int hauteur, int equipe, Plateau plateau) {
		this.coord = new Coordonnees(largeur, hauteur);
		this.coord.setHauteur(hauteur);
		this.coord.setLargeur(largeur);
		this.equipe = equipe;
		this.plateau = plateau;
	}

	public String toString() {
		return getType() + this.getIdUnite();
	}

	/**
	 * Met a jour l'energie du robot en fonction de l'energie passee en
	 * parametre.
	 * 
	 * @param degats
	 */
	public void perdEnergie(int energiePerdue) {
		energie = energie - energiePerdue;
	}

	/**
	 * Actualise l'energie du robot quand ce dernier passe sur une mine.
	 */
	public void subitMine() {
		perdEnergie(Constantes.DEGAT_MINE);
	}

	/**
	 * Met a jouer l'energie du robot en fonction du Robot robot passe en
	 * parametre.
	 * 
	 * @param robot
	 */
	public void subitDegats(Robot robot) {
		if (robot instanceof Tireur)
			perdEnergie(Constantes.DEGAT_TIREUR);
		if (robot instanceof Char)
			perdEnergie(Constantes.DEGAT_CHAR);
		if (robot instanceof Piegeur)
			perdEnergie(Constantes.DEGAT_MINE);
	}

	public void setCoordonnees(int largeur, int hauteur) {
		coord.setLargeur(largeur);
		coord.setHauteur(hauteur);
	}

	public void setCoordonnees(Coordonnees coord) {
		this.coord = coord;
	}

	public Coordonnees getCoordonnees() {
		return coord;
	}

	public int getEnergie() {
		return energie;
	}

	public void setEnergie(int energie) {
		this.energie = energie;
	}

	public int getEquipe() {
		return equipe;
	}

	public int getIdUnite() {
		return idUnite;
	}

	public void setIdUnite(int idUnite) {
		this.idUnite = idUnite;
	}

	public Plateau getPlateau() {
		return plateau;
	}

	public int getPorteeAtt() {
		return porteeAtt;
	}

	public void setPorteeAtt(int porteeAtt) {
		this.porteeAtt = porteeAtt;
	}

	public int getPorteeDep() {
		return porteeDep;
	}

	public void setPorteeDep(int porteeDep) {
		this.porteeDep = porteeDep;
	}

	public void enleverMine() {
		nbMines--;
	}

	public void setNbMines(int nbMines) {
		this.nbMines = nbMines;
	}

	public int getNbMines() {
		return nbMines;
	}

	/**
	 * Renvoie le coup d'un deplacement du Robot courant.
	 * 
	 * @return int
	 */
	public int getCoupDeplacement() {

		if (this instanceof Tireur)
			return Constantes.COUP_DEPLACEMENT_TIREUR;
		if (this instanceof Piegeur)
			return Constantes.COUP_DEPLACEMENT_PIEGEUR;
		else {
			return Constantes.COUP_DEPLACEMENT_CHAR;
		}
	}

	/**
	 * Renvoie le coup d'une attaque du Robot courant.
	 * 
	 * @return int
	 */
	public int getCoupAttaque() {

		if (this instanceof Tireur)
			return Constantes.COUP_ATTAQUE_TIREUR;
		if (this instanceof Piegeur)
			return Constantes.COUP_ATTAQUE_PIEGEUR;
		else {
			return Constantes.COUP_ATTAQUE_CHAR;
		}
	}

	/**
	 * Fixe l'energie maximale du Robot courant.
	 * 
	 * @param energieMax
	 */
	public void setEnergieMax(int energieMax) {
		this.energieMax = energieMax;
	}

	public int getEnergieMax() {
		return energieMax;
	}

	/**
	 * Retourne le type du Robot.
	 * 
	 * @return String
	 */
	abstract public String getType();

	/**
	 * Retourne vrai si le Robot peut tirer.
	 * 
	 * @return boolean
	 */
	abstract public boolean peutTirer();

	public int getX() {
		return coord.getLargeur();
	}

	public int getY() {
		return coord.getHauteur();
	}

	/**
	 * Determine si le robot est en �tat de jouer (d�placement ou attaque)
	 * @return boolean
	 */
	
	public boolean peutJouer() {
		if (this instanceof Tireur)
			return (this.energie > Constantes.COUP_ATTAQUE_TIREUR || this.energie > Constantes.COUP_DEPLACEMENT_TIREUR);
		else if (this instanceof Char)
			return (this.energie > Constantes.COUP_ATTAQUE_CHAR || this.energie > Constantes.COUP_DEPLACEMENT_CHAR);
		else
			return (this.energie > Constantes.COUP_ATTAQUE_PIEGEUR || this.energie > Constantes.COUP_DEPLACEMENT_PIEGEUR);
	}

}
