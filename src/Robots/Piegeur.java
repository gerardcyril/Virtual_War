package Robots;
import Jeu.Constantes;
import Plateau.Coordonnees;
import Plateau.Plateau;

@SuppressWarnings("serial")
public class Piegeur extends Robot {

	protected static int nbUniteEquipe1;
	protected static int nbUniteEquipe2;
	

	/**
	 * Permet de creer un piegeur avec des Coordonnees une equipe et un plateau.
	 * 
	 * @param coord
	 * @param equipe
	 * @param plateau
	 */
	public Piegeur(Coordonnees coord, int equipe, Plateau plateau) {
		super(coord, equipe, plateau);
		if (equipe == 1) {
			nbUniteEquipe1++;
			this.setIdUnite(nbUniteEquipe1);
		}
		if (equipe == 2) {
			nbUniteEquipe2++;
			this.setIdUnite(nbUniteEquipe2);
		}
		this.setEnergie(Constantes.ENERGIE_MAX_PIEGEUR);
		this.setEnergieMax(Constantes.ENERGIE_MAX_PIEGEUR);
		this.setPorteeAtt(Constantes.PORTEE_ATT_PIEGEUR);
		this.setPorteeDep(Constantes.PORTEE_DEP_PIEGEUR);
		this.setNbMines(Constantes.NB_MAX_MINES);
	}

	/**
	 * Permet de creer un Piegeur de Coordonnes(largeur, hauteur), une equipe et
	 * un plateau.
	 * 
	 * @param largeur
	 * @param hauteur
	 * @param equipe
	 * @param plateau
	 */
	public Piegeur(int largeur, int hauteur, int equipe, Plateau plateau) {
		super(largeur, hauteur, equipe, plateau);
		if (equipe == 1) {
			nbUniteEquipe1++;
			this.setIdUnite(nbUniteEquipe1);
		}
		if (equipe == 2) {
			nbUniteEquipe2++;
			this.setIdUnite(nbUniteEquipe2);
		}
		this.setEnergie(Constantes.ENERGIE_MAX_PIEGEUR);
		this.setEnergieMax(Constantes.ENERGIE_MAX_PIEGEUR);
		this.setPorteeAtt(Constantes.PORTEE_ATT_PIEGEUR);
		this.setPorteeDep(Constantes.PORTEE_DEP_PIEGEUR);
		this.setNbMines(Constantes.NB_MAX_MINES);
	}

	/**
	 * Retourne vrai si un tireur a assez d'energie pour tier, faux sinon.
	 */
	public boolean peutTirer() {
		return (getEnergie() > Constantes.COUP_ATTAQUE_PIEGEUR && getNbMines() > 0);
	}

	/**
	 * Retourne le type du Piegeur courant en minuscule pour l'equipe 1 et en
	 * majuscule pour l'equipe 2.
	 */
	public String getType() {
		if (this.getEquipe() == 1)
			return "p";
		else {
			return "P";
		}
	}

}
