package Robots;
import Jeu.Constantes;
import Plateau.Coordonnees;
import Plateau.Plateau;

@SuppressWarnings("serial")
public class Char extends Robot {

	protected static int nbUniteEquipe1;
	protected static int nbUniteEquipe2;

	/**
	 * Construit un Char avec une coordonnee, une equipe et un plateau.
	 * 
	 * @param coord
	 * @param equipe
	 * @param plateau
	 */
	public Char(Coordonnees coord, int equipe, Plateau plateau) {
		super(coord, equipe, plateau);
		if (equipe == 1) {
			nbUniteEquipe1++;
			this.setIdUnite(nbUniteEquipe1);
		}
		if (equipe == 2) {
			nbUniteEquipe2++;
			this.setIdUnite(nbUniteEquipe2);
		}
		this.setEnergie(Constantes.ENERGIE_MAX_CHAR);
		this.setEnergieMax(Constantes.ENERGIE_MAX_CHAR);
		this.setPorteeAtt(Constantes.PORTEE_ATT_CHAR);
		this.setPorteeDep(Constantes.PORTEE_DEP_CHAR);
	}

	/**
	 * Construit un Char de Coordonnees(largeur, hauteur), une equipe et un
	 * plateau.
	 * 
	 * @param largeur
	 * @param hauteur
	 * @param equipe
	 * @param plateau
	 */
	public Char(int largeur, int hauteur, int equipe, Plateau plateau) {
		super(largeur, hauteur, equipe, plateau);
		if (equipe == 1) {
			nbUniteEquipe1++;
			this.setIdUnite(nbUniteEquipe1);
		}
		if (equipe == 2) {
			nbUniteEquipe2++;
			this.setIdUnite(nbUniteEquipe2);
		}
		this.setEnergie(Constantes.ENERGIE_MAX_CHAR);
		this.setEnergieMax(Constantes.ENERGIE_MAX_CHAR);
		this.setPorteeAtt(Constantes.PORTEE_ATT_CHAR);
		this.setPorteeDep(Constantes.PORTEE_DEP_CHAR);
	}

	/* (non-Javadoc)
	 * @see Robot#peutTirer()
	 */
	public boolean peutTirer() {
		return (getEnergie() > Constantes.COUP_ATTAQUE_CHAR);
	}

	/* (non-Javadoc)
	 * @see Robot#getType()
	 */
	public String getType() {
		if (this.getEquipe() == 1)
			return "c";
		else {
			return "C";
		}
	}
}
