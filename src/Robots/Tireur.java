package Robots;
import Jeu.Constantes;
import Plateau.Coordonnees;
import Plateau.Plateau;

@SuppressWarnings("serial")
public class Tireur extends Robot {

	protected static int nbUniteEquipe1;
	protected static int nbUniteEquipe2;
	
	/**
	 * Construit un tireur avec une coordonnee, une equipe et un plateau.
	 * @param coord
	 * @param equipe
	 * @param plateau
	 */
	public Tireur(Coordonnees coord, int equipe, Plateau plateau) {
		super(coord, equipe, plateau);
		if (equipe == 1) {
			nbUniteEquipe1++;
			this.setIdUnite(nbUniteEquipe1);
		}
		if (equipe == 2) {
			nbUniteEquipe2++;
			this.setIdUnite(nbUniteEquipe2);
		}
		this.setEnergie(Constantes.ENERGIE_MAX_TIREUR);
		this.setEnergieMax(Constantes.ENERGIE_MAX_TIREUR);
		this.setPorteeAtt(Constantes.PORTEE_ATT_TIREUR);
		this.setPorteeDep(Constantes.PORTEE_DEP_TIREUR);
	}

	/**
	 * Construit un tireur avec une largeur, une hauteur, une equipe et un plateau.
	 * @param largeur
	 * @param hauteur
	 * @param equipe
	 * @param plateau
	 */
	public Tireur(int largeur, int hauteur, int equipe, Plateau plateau) {
		super(largeur, hauteur, equipe, plateau);
		if (equipe == 1) {
			nbUniteEquipe1++;
			this.setIdUnite(nbUniteEquipe1);
		}
		if (equipe == 2) {
			nbUniteEquipe2++;
			this.setIdUnite(nbUniteEquipe2);
		}
		this.setEnergie(Constantes.ENERGIE_MAX_TIREUR);
		this.setEnergieMax(Constantes.ENERGIE_MAX_TIREUR);
		this.setPorteeAtt(Constantes.PORTEE_ATT_TIREUR);
		this.setPorteeDep(Constantes.PORTEE_DEP_TIREUR);
	}

	/* (non-Javadoc)
	 * @see Robot#peutTirer()
	 */
	public boolean peutTirer() {
		return (getEnergie() > Constantes.COUP_ATTAQUE_TIREUR);
	}

	/* (non-Javadoc)
	 * @see Robot#getCoupDeplacement()
	 */
	public int getCoupDeplacement() {
		return Constantes.COUP_DEPLACEMENT_TIREUR;
	}

	/**
	 * Retourne les degats d'un tireur.
	 * @return int
	 */
	public int getDegatTir() {
		return Constantes.DEGAT_TIREUR;
	}

	/* (non-Javadoc)
	 * @see Robot#getType()
	 */
	public String getType() {
		if (this.getEquipe() == 1)
			return "t";
		else {
			return "T";
		}
	}
}
